from pyramid.view import view_config
from pyramid.httpexceptions import HTTPFound
from pyramid.httpexceptions import HTTPNotFound
from pyramid.httpexceptions import HTTPForbidden
from pyramid.httpexceptions import HTTPUnauthorized
from pyramid.httpexceptions import HTTPBadRequest
from pyramid.httpexceptions import HTTPServiceUnavailable
from pyramid.security import remember, forget
from pyramid.response import Response, FileResponse

import sys
import os
import json
import random
import string
import datetime
from datetime import timedelta
import time
from copy import deepcopy
import urllib3
import logging
import socket
import hashlib
import yaml
import base64
import pkg_resources
import humanfriendly

from bson.json_util import dumps
from bson.objectid import ObjectId

import jwt
from jsonschema import validate

# from cryptography.hazmat.backends import default_backend
# from cryptography.hazmat.primitives.asymmetric import rsa

from prometheus_client import multiprocess
from prometheus_client.exposition import generate_latest
from prometheus_client import Gauge
from prometheus_client import Counter
from prometheus_client import Histogram
from prometheus_client import CollectorRegistry

from godocker.utils import is_array_child_task, is_array_task
import godocker.utils as godutils

from pymongo import DESCENDING, ASCENDING

# if sys.version_info > (3,):
#     import configparser
# else:
#     import ConfigParser


def redis_call(request, f, *args, **kwargs):
    '''
    Executes a redis call, and retries connection and command in case of failure.

    Example: redis_call(self.r.hset, field, keys, 1)
    '''
    count = 0
    max_retries = 5
    while True:
        try:
            return f(*args, **kwargs)
        except Exception as e:
            logging.exception(str(e))
            count += 1
            # re-raise the ConnectionError if we've exceeded max_retries
            if count > max_retries:
                raise
            logging.warn('Redis:Retrying in {} seconds'.format(count))
            time.sleep(count)
            request.registry.db_redis = request.registry.redis_get_conn(request.registry.god_config)


def _get_user_db(request, user_id):
    '''
    Get user info from db, create/update it from fetched info with auth_policy
    '''
    user = request.registry.auth_policy.get_user(user_id)
    if not user:
        return None
    dbuser = request.registry.db_mongo['users'].find_one({'id': user['id']})
    if not dbuser:
        private_key = ''
        public_key = ''
        request.registry.db_mongo['users'].insert({
            'id': user['id'],
            'last': datetime.datetime.now(),
            'uid': user['uidNumber'],
            'gid': user['gidNumber'],
            'sgids': user['sgids'],
            'homeDirectory': user['homeDirectory'],
            'email': user['email'],
            'credentials': {
                'apikey': ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(10)),
                'private': private_key,
                'public': public_key
            },
            'usage': request.registry.auth_policy.get_quotas(user['id'])
        })
        dbuser = request.registry.db_mongo['users'].find_one({'id': user['id']})
    else:
        upgrade = False
        if 'sgids' not in dbuser:
            upgrade = True
        if (datetime.datetime.now() - dbuser['last']).days > 1 or upgrade:
            # Not updated for one day, update use info
            request.registry.db_mongo['users'].update(
                {'id': user['id']},
                {'$set': {
                    'last': datetime.datetime.now(),
                    'uid': user['uidNumber'],
                    'gid': user['gidNumber'],
                    'sgids': user['sgids'],
                    'homeDirectory': user['homeDirectory'],
                    'email': user['email']}
                }
            )
            dbuser = request.registry.db_mongo['users'].find_one({'id': user['id']})
    return dbuser


def _reload_config(request):
    '''
    Reload config if last reload command if recent
    '''
    cfg = request.registry.god_config
    config_last_update = redis_call(request, request.registry.db_redis.get, cfg['redis_prefix'] + ':config:last')
    if config_last_update is not None:
        config_last_update = float(config_last_update)
        if config_last_update > request.registry.config_last_loaded:
            logging.warn('Reloading configuration')
            with open(request.registry.config_file, 'r') as ymlfile:
                request.registry.god_config = yaml.load(ymlfile)
                dt = datetime.datetime.now()
                request.registry.config_last_loaded = time.mktime(dt.timetuple())


def _in_maintenance(request):
    cfg = request.registry.god_config
    maintenance = redis_call(request, request.registry.db_redis.get, cfg['redis_prefix'] + ':maintenance')
    if maintenance is not None and maintenance == 'on':
        return True
    return False


def _get_user(request, user_id):
    '''
    Get user from session or database
    '''
    user = request.registry.db_mongo['users'].find_one({'id': user_id})
    return user


def _auth_user_filter(request, user_id, user_filter):
    '''
    Add filter on user if connected user is not an admin
    '''
    admins = request.registry.settings['admin'].split(',')
    if user_id in admins:
        return user_filter
    else:
        user_filter['user.id'] = user_id
        return user_filter


def _is_admin(request, user_id):
    '''
    checks if user is an administrator
    '''
    admins = request.registry.settings['admin'].split(',')
    return user_id in admins


def is_authenticated(request, mode=None):
    # Try to get Authorization bearer with jwt encoded user information
    if 'HTTP_REMOTE_USER' in request.environ:
        if 'allow_proxy_remote_user' not in request.registry.god_config:
            logging.error('HTTP_REMOTE_USER set in headers but not allowed by config')
            return None
        if not request.registry.god_config['allow_proxy_remote_user']:
            logging.error('HTTP_REMOTE_USER set in headers but not allowed by config')
            return None
        remote_user = request.environ['HTTP_REMOTE_USER']
        user_db = _get_user_db(request, remote_user)
        return user_db
    if request.authorization is not None:
        try:
            auth_type = None
            bearer = None
            try:
                (auth_type, bearer) = request.authorization
            except:
                logging.debug("Invalid authorization header " + str(request.authorization))
            if auth_type == 'token':
                # Token auth needs a mode (task, user, ...)
                # Used for remote tasks, limited to task creation, etc.. no user access allowed
                if mode is None or mode not in ['task']:
                    return None
                app_db = request.registry.db_mongo['remote_app'].find_one({'token': bearer})
                if app_db is None:
                    return None
                user_db = request.registry.db_mongo['users'].find_one({'id': app_db['user']})
                return user_db
            secret = request.registry.god_config['secret_passphrase']
            # If decode ok and not expired
            user = jwt.decode(bearer, secret, audience='urn:godocker/api')
            if 'apikey' in user['user']:
                # Check apikey is still valid
                user_db = request.registry.db_mongo['users'].find_one({'id': user['user']['id']})
                if user_db['credentials']['apikey'] != user['user']['apikey']:
                    return None
            return user['user']
        except Exception:
            prometheus_api_inc(request, 'auth_error')
            return None
    user_id = request.authenticated_userid
    if user_id:
        user = request.registry.db_mongo['users'].find_one({'id': user_id})
        return user
    else:
        prometheus_api_inc(request, 'auth_error')
        return None


def _user_in_project(request, user_id, project_id):
    '''
    Checks if user is a member of project to manage a task (own or other user in same project)

    :param request: pyramid request object
    :type request: pyramid request
    :param user_id: user identifier
    :type user_id: str
    :param project_id: project identifier
    :type project_id: str
    :return: True if in project, else False
    '''
    user = request.registry.db_mongo['users'].find_one({'id': user_id})
    user_ids = [user_id]
    if user['gid']:
        user_ids.append(str(user['gid']))
    if user['sgids']:
        for sgid in user['sgids']:
            user_ids.append(str(sgid))
    cfg = request.registry.god_config
    if 'share_project_tasks' not in cfg or not cfg['share_project_tasks']:
        return False
    project = request.registry.db_mongo['projects'].find_one({'id': project_id})
    if not project:
        return False
    for member in project['members']:
        if member in user_ids:
            return True
    return False


def _is_user_task_owner(request, user_id, task):
    '''
    Checks if user is admin, task owner or member of the same project

    :param request: pyramid request object
    :type request: pyramid request
    :param user_id: user identifier
    :type user_id: str
    :param task: task object from db
    :type task: Task
    :return: True if one condition, else return False
    '''
    if _is_admin(request, user_id):
        return True
    if task['user']['id'] == user_id:
        return True
    if task['user']['project'] != 'default' and _user_in_project(request, user_id, task['user']['project']):
        return True
    return False


@view_config(route_name='home')
def my_view(request):
    pyramid_env = os.environ.get('PYRAMID_ENV', 'dev')
    if pyramid_env == 'prod':
        return HTTPFound(request.static_url('godweb:webapp/dist/'))
    else:
        return HTTPFound(request.static_url('godweb:webapp/app/'))


@view_config(route_name='api_version', renderer='json')
def api_version(request):
    return {'go-d-web': pkg_resources.get_distribution('godweb').version, 'go-d-sched': pkg_resources.get_distribution('godsched').version}


def prometheus_api_inc(request, method):
    '''
    Increment API calls for prometheus
    '''
    try:
        if 'prometheus' not in request.registry.settings or request.registry.settings['prometheus'] == '0':
            return
    except Exception:
        return
    if request.registry.p_api_requests is None:
        request.registry.p_api_requests = Counter('godocker_api_requests',
                                           'Number of API calls', ["method"])
    request.registry.p_api_requests.labels(method).inc()


@view_config(route_name='api_prometheus', renderer='json', request_method='POST')
def api_prometheus(request):
    '''
    .. http:post:: /api/1.0/prometheus

       Push prometheus metrics, admin only via prometheus_key config parameter

       :statuscode 200: no error
    '''
    cfg = request.registry.god_config
    body = request.body
    if sys.version_info >= (3,):
        body = request.body.decode()

    form = json.loads(body, encoding=request.charset)
    if 'key' not in form or form['key'] == '' or form['key'] != cfg['prometheus_key']:
        return HTTPForbidden('Invalid key')
    del form['key']
    for stat in form['stat']:
        if stat['name'] not in request.registry.p_exporter or request.registry.p_exporter[stat['name']] is None:
            request.registry.p_exporter[stat['name']] = Histogram(stat['name'], stat['name'], ['godocker_host'])
        if 'host' not in stat:
            stat['host'] = 'none'
        request.registry.p_exporter[stat['name']].labels(stat['host']).observe(stat['value'])
    return {}


@view_config(route_name='prometheus', renderer='string', request_method='GET')
def prometheus(request):
    '''
    .. http:get:: /metrics

       Prometheus metrics

       :statuscode 200: no error
    '''
    request.response.content_type = 'text/plain; version=0.0.4; charset=utf-8'
    cfg = request.registry.god_config
    running = redis_call(request, request.registry.db_redis.llen, cfg['redis_prefix'] + ':jobs:running')
    pending = redis_call(request, request.registry.db_redis.get, cfg['redis_prefix'] + ':jobs:queued')
    if pending is None:
        pending = 0
    total = redis_call(request, request.registry.db_redis.get, cfg['redis_prefix'] + ':jobs')
    if total is None:
        total = 0
    if request.registry.p_pending is None:
        request.registry.p_pending = Gauge('godocker_jobs_queued', 'Number of queued jobs', multiprocess_mode='all')
    request.registry.p_pending.set(int(pending))
    if request.registry.p_running is None:
        request.registry.p_running = Gauge('godocker_jobs_running', 'Number of running jobs', multiprocess_mode='all')
    request.registry.p_running.set(int(running))
    if request.registry.p_total is None:
        request.registry.p_total = Gauge('godocker_jobs_total', 'Number of total jobs', multiprocess_mode='all')
    request.registry.p_total.set(int(total))
    registry = CollectorRegistry()
    multiprocess.MultiProcessCollector(registry)
    return generate_latest(registry)


@view_config(route_name='user_list', renderer='json', request_method='GET')
def user_list(request):
    '''
    .. http:get:: /api/1.0/user (Admin only)

       List users

       :<json dict: user data to update
       :>json dict: user data
       :statuscode 200: no error
    '''
    session_user = is_authenticated(request)
    if not session_user:
        return HTTPUnauthorized('Not authorized to access this resource')

    if not _is_admin(request, session_user['id']):
        return HTTPUnauthorized('Not authorized to access this resource')

    users = request.registry.db_mongo['users'].find()
    result = []
    for user in users:
        result.append(user)
    return result


@view_config(route_name='user_info', renderer='json', request_method='PUT')
def user_info_edit(request):
    '''
    .. http:put:: /api/1.0/user/(str:id)

       Update logged in user information (self or admin)

       :<json dict: user data to update
       :>json dict: user data
       :statuscode 200: no error
    '''
    session_user = is_authenticated(request)
    if not session_user:
        return HTTPUnauthorized('Not authorized to access this resource')

    requested_user = request.matchdict['id']
    if not (_is_admin(request, session_user['id']) or session_user['id'] == requested_user):
        return HTTPUnauthorized('Not authorized to access this resource')

    user = request.registry.db_mongo['users'].find_one({'id': requested_user})
    if not user:
        return HTTPNotFound()
    body = request.body
    if sys.version_info >= (3,):
        body = request.body.decode()
    form = json.loads(body, encoding=request.charset)

    if 'credentials' not in user:
        user['credentials'] = {}

    user['credentials']['apikey'] = form['credentials']['apikey']
    user['credentials']['public'] = form['credentials']['public']

    data_to_update = {
            'credentials.apikey': user['credentials']['apikey'],
            'credentials.public': user['credentials']['public'],
            }

    if _is_admin(request, session_user['id']):
        user['usage']['prio'] = form['usage']['prio']
        data_to_update['usage.prio'] = user['usage']['prio']
        if 'quota_time' in form['usage']:
            try:
                int(form['usage']['quota_time'])
            except Exception:
                return HTTPBadRequest('value must be an integer')
            user['usage']['quota_time'] = form['usage']['quota_time']
            data_to_update['usage.quota_time'] = user['usage']['quota_time']
        if 'quota_cpu' in form['usage']:
            try:
                int(form['usage']['quota_cpu'])
            except Exception:
                return HTTPBadRequest('value must be an integer')
            user['usage']['quota_cpu'] = form['usage']['quota_cpu']
            data_to_update['usage.quota_cpu'] = user['usage']['quota_cpu']
        if 'quota_ram' in form['usage']:
            try:
                int(form['usage']['quota_ram'])
            except Exception:
                return HTTPBadRequest('value must be an integer')
            user['usage']['quota_ram'] = form['usage']['quota_ram']
            data_to_update['usage.quota_ram'] = user['usage']['quota_ram']
        if 'quota_gpu' in form['usage']:
            try:
                int(form['usage']['quota_gpu'])
            except Exception:
                return HTTPBadRequest('value must be an integer')
            user['usage']['quota_gpu'] = form['usage']['quota_gpu']
            data_to_update['usage.quota_gpu'] = user['usage']['quota_gpu']

    request.registry.db_mongo['users'].update({'id': user['id']}, {'$set': data_to_update})

    return user


@view_config(route_name='user_usage', renderer='json', request_method='GET')
def user_usage(request):
    '''
    .. http:get:: /api/1.0/user/(str:id)/usage

       Get user usage (self or admin)

       :>json list: user usage {'cpu','ram','time'}
       :statuscode 200: no error
    '''
    session_user = is_authenticated(request)
    if not session_user:
        return HTTPUnauthorized('Not authorized to access this resource')

    requested_user = request.matchdict['id']
    if not (_is_admin(request, session_user['id']) or session_user['id'] == requested_user):
        return HTTPUnauthorized('Not authorized to access this resource')

    dt = datetime.datetime.now()
    cfg = request.registry.god_config
    usages = []
    for i in range(0, cfg['user_reset_usage_duration']):
        previous = dt - timedelta(days=i)
        date_key = str(previous.year) + '_' + str(previous.month) + '_' + str(previous.day)
        if redis_call(request, request.registry.db_redis.exists, cfg['redis_prefix'] + ':user:' + requested_user + ':cpu:' + date_key):
                cpu = redis_call(request, request.registry.db_redis.get, cfg['redis_prefix'] + ':user:' + requested_user + ':cpu:' + date_key)
                if not cpu:
                    cpu = 0
                gpu = redis_call(request, request.registry.db_redis.get, cfg['redis_prefix'] + ':user:' + requested_user + ':gpu:' + date_key)
                if not gpu:
                    gpu = 0
                ram = redis_call(request, request.registry.db_redis.get, cfg['redis_prefix'] + ':user:' + requested_user + ':ram:' + date_key)
                if not ram:
                    ram = 0
                duration = redis_call(request, request.registry.db_redis.get, cfg['redis_prefix'] + ':user:' + requested_user + ':time:' + date_key)
                if not duration:
                    duration = 0
                usages.append({'cpu': int(cpu),
                                'ram': int(ram),
                                'gpu': int(gpu),
                                'time': float(duration),
                                'date': date_key
                            })
    return usages


@view_config(route_name='user_info', renderer='json', request_method='GET')
def user_info(request):
    '''
    .. http:get:: /api/1.0/user/(str:id)

       Get user information (self or admin)

       :>json dict: user data
       :statuscode 200: no error
    '''
    session_user = is_authenticated(request)
    if not session_user:
        return HTTPUnauthorized('Not authorized to access this resource')

    requested_user = request.matchdict['id']
    if not (_is_admin(request, session_user['id']) or session_user['id'] == requested_user):
        return HTTPUnauthorized('Not authorized to access this resource')

    user = request.registry.db_mongo['users'].find_one({'id': requested_user})
    if not user:
        return HTTPNotFound()

    projects = request.registry.db_mongo['projects'].find_one({'owner': user['id']})
    if projects:
        user['project_owner'] = True
    else:
        user['project_owner'] = False

    user['admin'] = False
    if _is_admin(request, user['id']):
        user['admin'] = True
    return user


@view_config(route_name='user_logged', renderer='json', request_method='GET')
def user_logged(request):
    user = is_authenticated(request)
    if not user:
        return HTTPNotFound('User not logged')
    user['admin'] = False
    if _is_admin(request, user['id']):
        user['admin'] = True
    return user


@view_config(context='velruse.AuthenticationComplete')
def login_complete_view(request):
    context = request.context
    user_id = None
    if context.profile['preferredUsername']:
        user_id = context.profile['preferredUsername']
    else:
        user_id = context.profile['accounts'][0]['username']

    email = None
    if context.profile['verifiedEmail']:
        email = context.profile['verifiedEmail']
    result = {
        'id': user_id,
        'email': email,
        'provider_type': context.provider_type,
        'provider_name': context.provider_name,
        'profile': context.profile,
        'credentials': context.credentials,
    }
    cfg = request.registry.god_config
    secret = cfg['secret_passphrase']
    session_timeout = 172800
    if 'session_timeout' in cfg and cfg['session_timeout']:
        session_timeout = cfg['session_timeout']
    token = jwt.encode({'user': result,
                        'exp': datetime.datetime.utcnow() + datetime.timedelta(seconds=session_timeout),
                        'aud': 'urn:godocker/api'}, secret)
    return HTTPFound(request.static_url('godweb:webapp/' + request.registry.runenv + '/') + "index.html#login?guest_token=" + token)


@view_config(route_name='guest_status', renderer='json', request_method='POST')
def guest_status(request):
    '''
    Update status of a user, user is in body
    '''
    user = is_authenticated(request)
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
    if not _is_admin(request, user['id']):
        return HTTPForbidden('Not authorized to access this resource')
    body = request.body
    if sys.version_info >= (3,):
        body = request.body.decode()
    form = json.loads(body, encoding=request.charset)
    request.registry.db_mongo['users'].update({'id': form['id']}, {'$set': {'guest.active': form['guest']['active']}})
    return {}


@view_config(route_name='guest_bind', renderer='json', request_method='POST')
def guest_bind(request):
    cfg = request.registry.god_config
    if 'guest_allow' not in cfg or not cfg['guest_allow']:
        return HTTPForbidden()
    secret = cfg['secret_passphrase']
    user = None
    body = request.body
    if sys.version_info >= (3,):
        body = request.body.decode()
    form = json.loads(body, encoding=request.charset)
    try:
        decoded_msg = jwt.decode(form['token'], secret, audience='urn:godocker/api')
        user = decoded_msg['user']
    except Exception:
        return HTTPForbidden()

    guest_id = user['id']
    userhash = hashlib.md5(user['id']).hexdigest()
    user['id'] = userhash
    dbuser = request.registry.db_mongo['users'].find_one({'id': userhash})
    if not dbuser:
        auth_policy = request.registry.auth_policy
        user_bind = auth_policy.get_user(cfg['guest_bind'])
        homedir = None

        if cfg['guest_home_root'] == 'default':
            homedir = os.path.join(user_bind['homeDirectory'], userhash)
        else:
            homedir = os.path.join(cfg['guest_home_root'], userhash)

        private_key = ''
        public_key = ''
        userdb = {
            'id': userhash,
            'last': datetime.datetime.now(),
            'uid': user_bind['uidNumber'],
            'gid': user_bind['gidNumber'],
            'sgids': user_bind['sgids'],
            'homeDirectory': homedir,
            'email': user['email'],
            'credentials': {
                'apikey': ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(10)),
                'private': private_key,
                'public': public_key
            },
            'guest': {
                'active': False,
                'is_guest': True,
                'bind': cfg['guest_bind']
            },
            'usage': auth_policy.get_quotas(cfg['guest_bind'], guest=guest_id)
            }
        request.registry.db_mongo['users'].insert(userdb)
        user = {
                  'id': userdb['id'],
                  'uidNumber': userdb['uid'],
                  'gidNumber': userdb['gid'],
                  'sgids': userdb['sgids'],
                  'email': userdb['email'],
                  'guest': True,
                  'guest_status': dbuser['guest']['active'],
                  'homeDirectory': userdb['homeDirectory']
                }

    headers = remember(request, user['id'])
    request.response.headerlist.extend(headers)
    user['admin'] = False
    user['guest'] = True
    user['guest_status'] = dbuser['guest']['active']
    session_timeout = 172800
    if 'session_timeout' in cfg and cfg['session_timeout']:
        session_timeout = cfg['session_timeout']

    token = jwt.encode({'user': user,
                        'exp': datetime.datetime.utcnow() + datetime.timedelta(seconds=session_timeout),
                        'aud': 'urn:godocker/api'}, secret)
    return {'user': user, 'token': token}


@view_config(route_name='user_bind', renderer='json', request_method='POST')
def user_bind(request):
    prometheus_api_inc(request, 'user_bind')
    auth_policy = request.registry.auth_policy
    user = {}
    try:
        body = request.body
        if sys.version_info >= (3,):
            body = request.body.decode()
        form = json.loads(body, encoding=request.charset)
        uid = form['uid']
        password = form['password']
        user = auth_policy.bind_credentials(uid, password)
        if user is None:
            return HTTPUnauthorized('Invalid credentials')
        dbuser = request.registry.db_mongo['users'].find_one({'id': user['id']})
        if not dbuser:
            # print str(dbuser)
            # Create a SSH user pair key
            # private_key = rsa.generate_private_key(
            #                            public_exponent=65537,
            #                            key_size=2048,
            #                            backend=default_backend()
            #                            )
            # public_key = private_key.public_key()
            # For debug
            private_key = ''
            public_key = ''
            request.registry.db_mongo['users'].insert({
                'id': user['id'],
                'last': datetime.datetime.now(),
                'uid': user['uidNumber'],
                'gid': user['gidNumber'],
                'sgids': user['sgids'],
                'homeDirectory': user['homeDirectory'],
                'email': user['email'],
                'credentials': {
                    'apikey': ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(10)),
                    'private': private_key,
                    'public': public_key
                },
                'usage': auth_policy.get_quotas(user['id'])
                }
                )
        else:
            upgrade = False
            if 'sgids' not in dbuser:
                upgrade = True
            if (datetime.datetime.now() - dbuser['last']).days > 1 or upgrade:
                # Not updated for one day, update use info
                request.registry.db_mongo['users'].update(
                    {'id': user['id']},
                    {'$set': {
                            'last': datetime.datetime.now(),
                            'uid': user['uidNumber'],
                            'gid': user['gidNumber'],
                            'sgids': user['sgids'],
                            'homeDirectory': user['homeDirectory'],
                            'email': user['email']
                        }
                    }
                )

        headers = remember(request, user['id'])
        request.response.headerlist.extend(headers)
    except Exception as e:
        logging.error(str(e))
        user = is_authenticated(request)
        if not user:
            return HTTPUnauthorized('Wrong credentials or not logged in')

    user['admin'] = False
    if _is_admin(request, user['id']):
        user['admin'] = True

    projects = request.registry.db_mongo['projects'].find_one({'owner': user['id']})
    if projects:
        user['project_owner'] = True
    else:
        user['project_owner'] = False

    secret = request.registry.god_config['secret_passphrase']
    cfg = request.registry.god_config
    session_timeout = 172800
    if 'session_timeout' in cfg and cfg['session_timeout']:
        session_timeout = cfg['session_timeout']

    token = jwt.encode({'user': user,
                        'exp': datetime.datetime.utcnow() + datetime.timedelta(seconds=session_timeout),
                        'aud': 'urn:godocker/api'}, secret)
    return {'user': user, 'token': token}


@view_config(route_name='admin_maintenance', renderer='json', request_method='GET')
def admin_maintenance(request):
    '''
    .. http:get:: /admin/maintenance

       :>json list: general status and per node status
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
    '''
    cfg = request.registry.god_config
    status_global = redis_call(request, request.registry.db_redis.get, cfg['redis_prefix'] + ':maintenance')
    if status_global is None:
        status_global = 'off'
    nodes = redis_call(request, request.registry.db_redis.hkeys, cfg['redis_prefix'] + ':maintenance:nodes')
    status_nodes = []
    for node in nodes:
        status_node = redis_call(request, request.registry.db_redis.hget, cfg['redis_prefix'] + ':maintenance:nodes', node)
        status_nodes.append({'name': node, 'status': status_node})
    return {'status': {'general': status_global, 'nodes': status_nodes}}


@view_config(route_name='admin_set_maintenance', renderer='json', request_method='POST')
def admin_set_maintenance(request):
    '''
    .. http:post:: /admin/maintenance/(str:node)/(str:status)

       Admin only, set maintenance status for system or a node
       node: *all* for system, else node name
       When system is in maintenance, new tasks will be rejected with a 503 error.
       Existing tasks (running, pending, ...) are not impacted but pending tasks will not be scheduled.

       Note: for the moment, only system maintenance is available, setting a specific node will have
             no impact and should be used for information only. See issue #32.

       :>json list: general status and per node status
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
    '''
    user = is_authenticated(request)
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
    if not _is_admin(request, user['id']):
        return HTTPForbidden('Not authorized to access this resource')

    node = request.matchdict['node']
    status = request.matchdict['status']

    if status != 'on' and status != 'off':
        return HTTPForbidden('Wrong status, only on or off are valid status')

    cfg = request.registry.god_config
    if node == 'all':
        redis_call(request, request.registry.db_redis.set, cfg['redis_prefix'] + ':maintenance', status)
    else:
        redis_call(request, request.registry.db_redis.hset, cfg['redis_prefix'] + ':maintenance:nodes', node, status)

    return admin_maintenance(request)


@view_config(route_name='admin_status', renderer='json', request_method='GET')
def _admin_status(request):
    '''
    Admin only, get processes status
    '''
    user = is_authenticated(request)
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
    if not _is_admin(request, user['id']):
        return HTTPForbidden('Not authorized to access this resource')

    # cfg = request.registry.god_config
    status_manager = request.registry.status_manager
    if status_manager is None:
        return []
    else:
        return status_manager.status()


@view_config(route_name='admin_unwatch', renderer='json', request_method='DELETE')
def _admin_unwatch(request):
    '''
    Admin only, unwatch a process
    '''
    user = is_authenticated(request)
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
    if not _is_admin(request, user['id']):
        return HTTPForbidden('Not authorized to access this resource')
    host_to_unwatch = request.matchdict['id']
    cfg = request.registry.god_config
    redis_call(request, request.registry.db_redis.hdel, cfg['redis_prefix'] + ':procs', host_to_unwatch)
    return {'msg': host_to_unwatch + ' removed from watch list'}


@view_config(route_name='user_logout', renderer='json', request_method='GET')
def user_logout(request):
    # user_id = request.authenticated_userid
    headers = forget(request)
    request.response.headerlist.extend(headers)
    return {'msg': 'logout'}


@view_config(route_name='api_user_file', renderer='json', request_method='GET')
def api_user_file(request):
    '''
    .. http:get:: /api/1.0/user/(int:id)/files/*file

       List user files from personal storage if *file is empty or a subdirectory of task directory,
        else download file matching *file

       :>json list: List of files and directory in *file if *file is a directory
       ::>octet-stream: File content if *file is a file
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
       :statuscode 404: not found
       :statuscode 503: service unavailable

    '''
    if _in_maintenance(request):
        return HTTPServiceUnavailable()

    prometheus_api_inc(request, 'task_file')
    user = is_authenticated(request)
    if not user or user['id'] != request.matchdict['id']:
        return HTTPUnauthorized('Not authenticated or expired')

    file_path = '/'.join(str(i) for i in request.matchdict['file'])
    # Get user storage path
    user_info = {
        'id': 'user_' + user['id']
    }

    god_dir_path = request.registry.store.get_task_dir(user_info)
    if not os.path.exists(god_dir_path):
        raise HTTPNotFound()

    file_path = os.path.join(god_dir_path,
                            file_path)
    if not os.path.exists(file_path):
        raise HTTPNotFound()

    if os.path.isfile(file_path):
        file_size = os.stat(file_path).st_size
        # cfg = request.registry.god_config
        max_file_download = 0
        try:
            max_file_download = int(request.registry.settings['max_file_download'])
        except Exception as e:
            logging.warn('max_file_download not set or badly configured in config: ' + str(e))
        if max_file_download > 0 and file_size > max_file_download:
            return HTTPUnauthorized('File too large, download not authorized')
        return FileResponse(file_path,
                                request=request,
                                content_type='text/plain')
    file_dir_list = os.listdir(file_path)
    result = []
    for file_or_dir in file_dir_list:
        if file_or_dir == "godocker.sh":
            continue
        if os.path.isfile(os.path.join(file_path, file_or_dir)):
            result.append({'name': file_or_dir,
                            'type': 'file',
                            'size': os.path.getsize(os.path.join(file_path, file_or_dir))})
        else:
            result.append({'name': file_or_dir,
                            'type':
                            'dir'})
    return result


@view_config(route_name='api_task_share', renderer='json', request_method='GET')
def api_task_share(request):
    '''
    .. http:get:: /api/1.0/task/(int:task)/share

       Generate a token to share task with other people

       :>json dict: dict containing token to use with /api/1.0/task/XXX?share=token
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
       :statuscode 404: not found
       :statuscode 503: service unavailable

    '''
    if _in_maintenance(request):
        return HTTPServiceUnavailable()

    prometheus_api_inc(request, 'task_share')

    user = is_authenticated(request, mode="task")
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')

    task_filter = {'id': int(request.matchdict['task'])}
    task = request.registry.db_mongo['jobsover'].find_one(task_filter)
    if not task:
        return HTTPNotFound()

    if not _is_user_task_owner(request, user['id'], task):
        return HTTPForbidden('User not member of task project')

    secret = request.registry.god_config['secret_passphrase']
    token = jwt.encode({'task_id': task['id'],
                        'aud': 'urn:godocker/api'}, secret)
    request.registry.db_mongo['jobsover'].update({'id': task['id']}, {'$set': {'status.is_shared': True, 'status.shared_token': token}})
    return {'token': token}


@view_config(route_name='api_task_unshare', renderer='json', request_method='GET')
def api_task_unshare(request):
    '''
    .. http:get:: /api/1.0/task/(int:task)/unshare

       Generate a token to share task with other people

       :>json dict: dict containing token to use with /api/1.0/task/XXX?share=token
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
       :statuscode 404: not found
       :statuscode 503: service unavailable

    '''
    if _in_maintenance(request):
        return HTTPServiceUnavailable()

    prometheus_api_inc(request, 'task_share')

    user = is_authenticated(request, mode="task")
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
    task_filter = {'id': int(request.matchdict['task'])}
    task = request.registry.db_mongo['jobsover'].find_one(task_filter)
    if not task:
        return HTTPNotFound()

    if not _is_user_task_owner(request, user['id'], task):
        return HTTPForbidden('User not member of task project')
    request.registry.db_mongo['jobsover'].update({'id': task['id']}, {'$set': {'status.is_shared': False}, '$unset': {'status.shared_token': ''}})
    return {'msg': 'unshared this task'}


@view_config(route_name='api_task_file_share', renderer='json', request_method='GET')
def api_task_file_share(request):
    '''
    .. http:get:: /api/1.0/task/(int:task)/share/{str:token}/files/*file

       List shared task files if *file is empty or a subdirectory of task directory,
        else download file matching *file

       :>json list: List of files and directory in *file if *file is a directory
       ::>octet-stream: File content if *file is a file
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
       :statuscode 404: not found
       :statuscode 503: service unavailable
    '''
    request.headers['X-GODOCKER-SHARE'] = request.matchdict['token']
    return api_task_file(request)


@view_config(route_name='api_task_file', renderer='json', request_method='GET')
def api_task_file(request):
    '''
    .. http:get:: /api/1.0/task/(int:task)/files/*file

       List task files if *file is empty or a subdirectory of task directory,
        else download file matching *file

        Accept request header X-GODOCKER-SHARE containing a share token to access files

       :>json list: List of files and directory in *file if *file is a directory
       ::>octet-stream: File content if *file is a file
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
       :statuscode 404: not found
       :statuscode 503: service unavailable

    '''
    if _in_maintenance(request):
        return HTTPServiceUnavailable()

    prometheus_api_inc(request, 'task_file')
    shared_task = False
    if 'X-GODOCKER-SHARE' in request.headers:
        share_token = request.headers['X-GODOCKER-SHARE']
        secret = request.registry.god_config['secret_passphrase']
        try:
            task_id = int(request.matchdict['task'])
            task_info = jwt.decode(share_token, secret, audience='urn:godocker/api')
            if task_info['task_id'] != task_id:
                return HTTPUnauthorized('Not authorized')
            shared_task = True
        except Exception as e:
            print(str(e))
            prometheus_api_inc(request, 'auth_error')
            return HTTPUnauthorized('Not authorized')

    user = None
    if not shared_task:
        user = is_authenticated(request, mode="task")
        if not user:
            return HTTPUnauthorized('Not authenticated or expired')

    task_filter = {'id': int(request.matchdict['task'])}
    task = request.registry.db_mongo['jobsover'].find_one(task_filter)
    if not task:
        # if not in over, check in running jobs
        task = request.registry.db_mongo['jobs'].find_one(task_filter)
    if task is None:
        return HTTPNotFound('Job does not exist')

    if not shared_task and not _is_user_task_owner(request, user['id'], task):
        return HTTPForbidden('User not member of task project')

    file_path = '/'.join(str(i) for i in request.matchdict['file'])
    god_dir_path = None
    for v in task['container']['volumes']:
        if v['name'] == 'go-docker':
            god_dir_path = v['path']
            break

    if god_dir_path is None:
        raise HTTPNotFound()

    file_path = os.path.join(god_dir_path,
                            file_path)
    if not os.path.exists(file_path):
        raise HTTPNotFound()

    if os.path.isfile(file_path):
        file_size = os.stat(file_path).st_size
        # cfg = request.registry.god_config
        max_file_download = 0
        try:
            max_file_download = int(request.registry.settings['max_file_download'])
        except Exception:
            logging.warn('max_file_download not set or badly configured in config')
        if max_file_download > 0 and file_size > max_file_download:
            return HTTPUnauthorized('File too large, download not authorized')
        return FileResponse(file_path,
                                request=request,
                                content_type='text/plain')
    file_dir_list = os.listdir(file_path)
    result = []
    for file_or_dir in file_dir_list:
        if file_or_dir == "godocker.sh":
            continue
        if os.path.isfile(os.path.join(file_path, file_or_dir)):
            result.append({'name': file_or_dir,
                            'type': 'file',
                            'size': os.path.getsize(os.path.join(file_path, file_or_dir))})
        else:
            result.append({'name': file_or_dir,
                            'type':
                            'dir'})
    return result


@view_config(route_name='api_usage', renderer='json', request_method='GET')
def api_usage(request):
    '''
    .. http:get:: /api/1.0/usage

       Get framework resource usage

       :>json list: list of nodes with resources
       :statuscode 200: no error
    '''
    usage = []
    for executor in request.registry.executors:
        usage += executor.usage()
    return usage


@view_config(route_name='api_config', renderer='json', request_method='GET')
def api_config(request):
    '''
    .. http:get:: /api/1.0/config

       Get general configuration

       :>json dict: configuration object with volumes etc...
       :statuscode 200: no error
    '''
    _reload_config(request)

    cfg = request.registry.god_config
    volumes = []

    if 'use_private_registry' not in cfg:
        cfg['use_private_registry'] = None

    for v in cfg['volumes']:
        volumes.append({'name': v['name'], 'acl': v['acl']})
    images = []
    for v in cfg['default_images']:
        is_default = False
        if 'default' in v:
            is_default = v['default']
        images.append({'name': v['name'], 'url': v['url'], 'default': is_default, 'interactive': v['interactive']})
    constraints = []
    for c in cfg['constraints']:
        constraints.append({'name': c['name'], 'value': c['value'].split(',')})

    networks = None
    if request.registry.network_plugin is not None:
        networks = request.registry.network_plugin.networks()

    projects = None
    if 'projects' in cfg:
        projects = cfg['projects']

    web_cfg = {
        'volumes': volumes,
        'cadvisor': {
            'port': cfg['cadvisor_port'],
            'url': cfg['cadvisor_url_part']
        },
        'defaults': {
            'cpu': cfg['defaults_cpu'],
            'ram': cfg['defaults_ram'],
            'allow_root': cfg['allow_root'],
            'allow_user_images': cfg['allow_user_images'],
            'use_private_registry': cfg['use_private_registry'],
            'tmpstorage': cfg['plugin_zfs'],
            'network': networks,
            'is_default_allowed': cfg['projects']['is_default_allowed']
        },
        'images': images,
        'live': {
            'status': cfg['live_events'],
            'url': cfg['live_events_url']
        },
        'constraints': constraints,
        'executors': cfg['executors'],
        'guest': request.registry.allow_auth,
        'bubble_chamber': request.registry.bubble_chamber,
        'projects': projects
    }

    executors_features = {}
    for executor in request.registry.executors:
        executors_features[executor.get_name()] = {}
        for feature in executor.features():
            executors_features[executor.get_name()][feature] = True
            executors_features[executor.get_name()]['docker'] = True
            if executor.get_name() in cfg['placement'] \
            and 'docker' in cfg['placement'][executor.get_name()] \
            and not cfg['placement'][executor.get_name()]['docker']:
                    executors_features[executor.get_name()]['docker'] = False
    web_cfg['executor_features'] = executors_features

    placements = []
    for executor in cfg['executors']:
        placement = executor
        if 'placement' in cfg and executor in cfg['placement']:
            placement_types = []
            if 'docker' in cfg['placement'][executor] and cfg['placement'][executor]['docker']:
                placement_types.append('docker')
            if 'native' in cfg['placement'][executor] and cfg['placement'][executor]['native']:
                placement_types.append('native')

            for placement in placement_types:
                if 'queues' not in cfg['placement'][executor]:
                    placements.append(executor + ':default:' + placement)
                else:
                    for queue in cfg['placement'][executor]['queues']:
                        placements.append(executor + ':' + queue + ':' + placement)
        else:
            placements.append(executor + ':default:docker')
    web_cfg['executors_placement'] = placements

    if 'ftp' in cfg:
        web_cfg['ftp'] = {
            'quota': humanfriendly.parse_size(cfg['ftp']['quota']),
            'endpoint': cfg['ftp']['public_endpoint']
            }
    if 'failure_policy' in cfg:
        web_cfg['defaults']['failure_policy'] = cfg['failure_policy']['strategy']
    else:
        web_cfg['defaults']['failure_policy'] = 0
    return web_cfg


@view_config(route_name='api_project_quota_reset', renderer='json', request_method='DELETE')
def api_project_quota_reset(request):
    '''
    .. http:delete:: /api/1.0/project/(str:id)/quota

       Reset project usage quota (admin)

       :statuscode 200: no error
    '''
    session_user = is_authenticated(request)
    if not session_user:
        return HTTPUnauthorized('Not authorized to access this resource')
    if not _is_admin(request, session_user['id']):
        return HTTPUnauthorized('Not authorized to access this resource')

    requested_project = request.matchdict['project']
    project = request.registry.db_mongo['projects'].find_one({'id': requested_project})
    if not project:
        return HTTPNotFound()

    dt = datetime.datetime.now()
    cfg = request.registry.god_config

    for i in range(0, cfg['user_reset_usage_duration']):
        previous = dt - timedelta(days=i)
        date_key = str(previous.year) + '_' + str(previous.month) + '_' + str(previous.day)
        if request.registry.db_redis.exists(cfg['redis_prefix'] + ':group:' + requested_project + ':cpu:' + date_key):
                redis_call(request, request.registry.db_redis.delete, cfg['redis_prefix'] + ':group:' + requested_project + ':cpu:' + date_key)
                redis_call(request, request.registry.db_redis.delete, cfg['redis_prefix'] + ':group:' + requested_project + ':gpu:' + date_key)
                redis_call(request, request.registry.db_redis.delete, cfg['redis_prefix'] + ':group:' + requested_project + ':ram:' + date_key)
                redis_call(request, request.registry.db_redis.delete, cfg['redis_prefix'] + ':group:' + requested_project + ':time:' + date_key)
    return {'msg': 'project quota reset done'}


@view_config(route_name='api_project_usage', renderer='json', request_method='GET')
def api_project_usage(request):
    '''
    .. http:get:: /api/1.0/project/(str:id)/usage

       Get project usage (in project or admin)

       :>json list: project usage {'cpu','ram','time'}
       :statuscode 200: no error
    '''
    session_user = is_authenticated(request)
    if not session_user:
        return HTTPUnauthorized('Not authorized to access this resource')

    requested_project = request.matchdict['project']

    projects = request.registry.db_mongo['projects'].find({'members': session_user['id']})
    user_in_project = False
    for project in projects:
        if project['id'] == requested_project:
            user_in_project = True
            break
    if not (_is_admin(request, session_user['id']) or user_in_project):
        return HTTPUnauthorized('Not authorized to access this resource')

    dt = datetime.datetime.now()
    cfg = request.registry.god_config
    usages = []
    for i in range(0, cfg['user_reset_usage_duration']):
        previous = dt - timedelta(days=i)
        date_key = str(previous.year) + '_' + str(previous.month) + '_' + str(previous.day)
        if request.registry.db_redis.exists(cfg['redis_prefix'] + ':group:' + requested_project + ':cpu:' + date_key):
                cpu = redis_call(request, request.registry.db_redis.get, cfg['redis_prefix'] + ':group:' + requested_project + ':cpu:' + date_key)
                if not cpu:
                    cpu = 0
                gpu = redis_call(request, request.registry.db_redis.get, cfg['redis_prefix'] + ':group:' + requested_project + ':gpu:' + date_key)
                if not gpu:
                    gpu = 0
                ram = redis_call(request, request.registry.db_redis.get, cfg['redis_prefix'] + ':group:' + requested_project + ':ram:' + date_key)
                if not ram:
                    ram = 0
                duration = redis_call(request, request.registry.db_redis.get, cfg['redis_prefix'] + ':group:' + requested_project + ':time:' + date_key)
                if not duration:
                    duration = 0
                usages.append({'cpu': int(cpu),
                                'ram': int(ram),
                                'gpu': int(gpu),
                                'time': float(duration),
                                'date': date_key
                            })

    return usages


@view_config(route_name='api_user_apikey', renderer='json', request_method='DELETE')
def api_user_apikey(request):
    '''
    .. http:delete:: /api/1.0/user/{str:user}/apikey

    Generate a new api key

    :>json dict: new apikey
    :statuscode 200: no error
    :statuscode 401: need login/authentication
    '''
    session_user = is_authenticated(request)
    if not session_user:
        return HTTPUnauthorized('Not authenticated or expired')
    requested_user = request.matchdict['id']
    if not (_is_admin(request, session_user['id']) or session_user['id'] == requested_user):
        return HTTPUnauthorized('Not authorized to access this resource')
    new_token = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(10))
    request.registry.db_mongo['users'].update({
                'id': requested_user},
                {'$set': {
                    'credentials.apikey': new_token
                    }
                })
    return {'apikey': new_token}


@view_config(route_name='api_user_project', renderer='json', request_method='GET')
def api_user_project_list(request):
    '''
    .. http:get:: /api/1.0/user/{str:user}/project

    Get the list of projects for user

    :>json list: List of projects
    :statuscode 200: no error
    :statuscode 401: need login/authentication
    :statuscode 403: not authorized
    '''
    session_user = is_authenticated(request)
    if not session_user:
        return HTTPUnauthorized('Not authenticated or expired')

    requested_user = request.matchdict['id']
    if not (_is_admin(request, session_user['id']) or session_user['id'] == requested_user):
        return HTTPUnauthorized('Not authorized to access this resource')

    user = request.registry.db_mongo['users'].find_one({'id': requested_user})

    project_filter = [user['id']]
    if user['gid']:
        project_filter.append(str(user['gid']))
    if user['sgids']:
        for sgid in user['sgids']:
            project_filter.append(str(sgid))

    projects = request.registry.db_mongo['projects'].find({'members': {'$in': project_filter}})

    res = []
    for project in projects:
        res.append(project)

    cfg = request.registry.god_config
    if cfg['projects']['is_default_allowed']:
        res.append({'id': 'default', 'prio': 50})
    return res


@view_config(route_name='api_project_list', renderer='json', request_method='GET')
def api_project_list(request):
    '''
    .. http:get:: /api/1.0/project

    Get the list of projects (Admin only) or projects owner by user

    :>json list: List of projects
    :statuscode 200: no error
    :statuscode 401: need login/authentication
    :statuscode 403: not authorized
    '''
    user = is_authenticated(request)
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
    # if not _is_admin(request, user['id']):
    #    return HTTPForbidden('Not authorized to access this resource')
    projects = []
    if _is_admin(request, user['id']):
        projects = request.registry.db_mongo['projects'].find()
    else:
        projects = request.registry.db_mongo['projects'].find({'owner': user['id']})

    res = []
    for project in projects:
        res.append(project)
    return res


@view_config(route_name='api_project', renderer='json', request_method='GET')
def api_project_get(request):
    '''
    .. http:get:: /api/1.0/project/{str:project}

    Get project details (Admin only)

    :>json dict:project
    :statuscode 200: no error
    :statuscode 401: need login/authentication
    :statuscode 403: not authorized
    '''
    user = is_authenticated(request)
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
    if not _is_admin(request, user['id']):
        return HTTPForbidden('Not authorized to access this resource')
    project = request.registry.db_mongo['projects'].find_one({'id': request.matchdict['project']})
    if not project:
        return HTTPNotFound()
    return project


@view_config(route_name='api_project', renderer='json', request_method='DELETE')
def api_project_delete(request):
    '''
    .. http:delete:: /api/1.0/project/{str:project}

    Delete project details (admin only)

    :statuscode 200: no error
    :statuscode 401: need login/authentication
    :statuscode 403: not authorized
    '''
    user = is_authenticated(request)
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
    if not _is_admin(request, user['id']):
        return HTTPForbidden('Not authorized to access this resource')
    request.registry.db_mongo['projects'].remove({'id': request.matchdict['project']})
    return {}


@view_config(route_name='api_project_list', renderer='json', request_method='POST')
def api_project_create(request):
    '''
    .. http:post:: /api/1.0/project

    Create a project (admin only)

    :<json dict:project
    :statuscode 200: no error
    :statuscode 401: need login/authentication
    :statuscode 403: not authorized
    '''
    user = is_authenticated(request)
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
    if not _is_admin(request, user['id']):
        return HTTPForbidden('Not authorized to access this resource')
    body = request.body
    if sys.version_info >= (3,):
        body = request.body.decode()
    form = json.loads(body, encoding=request.charset)
    if form['id'] == 'my':
        return HTTPBadRequest('Project name reserved')
    project_db = request.registry.db_mongo['projects'].find_one({'id': form['id']})
    if project_db is not None:
        return HTTPBadRequest('Project already exists')

    project_quota = request.registry.auth_policy.get_quotas(form['id'], is_group=True)
    project_limitation = request.registry.auth_policy.get_project_limitations(project_db)

    project = {
        'id': form['id'],
        'owner': form['owner'],
        'volumes': form['volumes'],
        'description': form['description'],
        'members': form['members'],
        'prio': project_quota['prio'],
        'quota_time': project_quota['quota_time'],
        'quota_cpu': project_quota['quota_cpu'],
        'quota_ram': project_quota['quota_ram'],
        'quota_gpu': project_quota['quota_gpu'],
        'limitation_cpu': project_limitation['limitation_cpu'],
        'limitation_ram': project_limitation['limitation_ram'],
        'limitation_gpu': project_limitation['limitation_gpu']
    }

    if 'prio' in form:
        try:
            int(form['prio'])
        except Exception:
            return HTTPBadRequest('value must be an integer')
        project['prio'] = form['prio']
    if 'quota_time' in form:
        try:
            int(form['quota_time'])
        except Exception:
            return HTTPBadRequest('value must be an integer')
        project['quota_time'] = form['quota_time']
    if 'quota_cpu' in form:
        try:
            int(form['quota_cpu'])
        except Exception:
            return HTTPBadRequest('value must be an integer')
        project['quota_cpu'] = form['quota_cpu']
    if 'quota_ram' in form:
        try:
            int(form['quota_ram'])
        except Exception:
            return HTTPBadRequest('value must be an integer')
        project['quota_ram'] = form['quota_ram']
    if 'quota_gpu' in form:
        try:
            int(form['quota_gpu'])
        except Exception:
            return HTTPBadRequest('value must be an integer')
        project['quota_gpu'] = form['quota_gpu']
    if 'limitation_cpu' in form:
        try:
            int(form['limitation_cpu'])
        except Exception:
            return HTTPBadRequest('value must be an integer')
        project['limitation_cpu'] = form['limitation_cpu']
    if 'limitation_ram' in form:
        try:
            int(form['limitation_ram'])
        except Exception:
            return HTTPBadRequest('value must be an integer')
        project['limitation_ram'] = form['limitation_ram']
    if 'limitation_gpu' in form:
        try:
            int(form['limitation_gpu'])
        except Exception:
            return HTTPBadRequest('value must be an integer')
        project['limitation_gpu'] = form['limitation_gpu']
    request.registry.db_mongo['projects'].insert(project)
    return project


@view_config(route_name='api_project', renderer='json', request_method='POST')
def api_project_edit(request):
    '''
    .. http:post:: /api/1.0/project/{str:project}

    Update a project (owner and admin only)

    Fields: description, members, prio, quota_time, quota_cpu, quota_ram, quota_gpu

    :<json dict:project
    :statuscode 200: no error
    :statuscode 401: need login/authentication
    :statuscode 403: not authorized
    '''
    user = is_authenticated(request)
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')

    body = request.body
    if sys.version_info >= (3,):
        body = request.body.decode()
    form = json.loads(body, encoding=request.charset)

    db_project = request.registry.db_mongo['projects'].find_one({'id': form['id']})
    is_project_owner = False
    if not db_project:
        return HTTPNotFound()
    if db_project['owner'] == user['id']:
        is_project_owner = True

    if not _is_admin(request, user['id']) and not is_project_owner:
        return HTTPForbidden('Not authorized to access this resource')

    try:
        int(form['quota_time'])
        int(form['quota_cpu'])
        int(form['quota_ram'])
        int(form['quota_gpu'])
        if 'limitation_cpu' in form:
            int(form['limitation_cpu'])
            int(form['limitation_ram'])
            int(form['limitation_gpu'])
    except Exception:
        return HTTPBadRequest('value must be an integer')

    cfg = request.registry.god_config
    project = {
        'description': form['description'],
        'members': form['members'],
    }
    if _is_admin(request, user['id']):
        project['volumes'] = form['volumes']
    elif is_project_owner:
        if 'projects' in cfg and 'owner_mount_any_dir' in cfg['projects'] and cfg['projects']['owner_mount_any_dir'] is True:
            project['volumes'] = form['volumes']

    if _is_admin(request, user['id']):
        project['owner'] = form['owner']
        project['prio'] = form['prio']
        project['quota_time'] = form['quota_time']
        project['quota_cpu'] = form['quota_cpu']
        project['quota_ram'] = form['quota_ram']
        project['quota_gpu'] = form['quota_gpu']
        if 'limitation_cpu' in form:
            project['limitation_cpu'] = form['limitation_cpu']
            project['limitation_ram'] = form['limitation_ram']
            project['limitation_gpu'] = form['limitation_gpu']

    request.registry.db_mongo['projects'].update({'id': form['id']}, {'$set': project})
    project['id'] = form['id']
    return project


@view_config(route_name='api_task_token', renderer='json', request_method='GET')
def api_task_token(request):
    '''
    .. http:get:: /api/1.0/task/(int:task)/token

    Get a JWT encoded task to securely exchange a task with other services.

    :>json dict: {'token': generated_token}
    :statuscode 200: no error
    :statuscode 401: need login/authentication
    '''
    prometheus_api_inc(request, 'task_token')
    user = is_authenticated(request)
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
    task_filter = {'id': int(request.matchdict['task'])}
    task_filter = _auth_user_filter(request, user['id'], task_filter)
    task = request.registry.db_mongo['jobs'].find_one(task_filter)
    if not task:
        task = request.registry.db_mongo['jobsover'].find_one(task_filter)
    if not task:
        return HTTPNotFound()
    secret = request.registry.god_config['secret_passphrase']
    cfg = request.registry.god_config
    del task['_id']
    session_timeout = 172800
    if 'session_timeout' in cfg and cfg['session_timeout']:
        session_timeout = cfg['session_timeout']

    token = jwt.encode({'task': task,
                        'exp': datetime.datetime.utcnow() + datetime.timedelta(seconds=session_timeout),
                        'aud': 'urn:godocker/api'}, secret)
    return {'token': token}


@view_config(route_name='api_auth', renderer='json', request_method='POST')
def api_auth(request):
    '''
    .. http:post:: /api/1.0/authenticate

       Authenticate user to get an Authorization bearer token used in future API calls

       :<json dict body: JSON credentials {'apikey': xx, 'user': user_id}
       :>json dict: {'token': generated_token}
       :statuscode 200: no error
       :statuscode 401: need login/authentication

    '''
    token = None
    try:
        body = request.body
        if sys.version_info >= (3,):
            body = request.body.decode()
        form = json.loads(body, encoding=request.charset)
        user_id = form['user']
        user = request.registry.db_mongo['users'].find_one({'id': user_id})
        if not user:
            return HTTPUnauthorized('User not defined, connect a first time via the Web interface to create user in the system.')
        if user['credentials']['apikey'] != form['apikey']:
            return HTTPUnauthorized('Wrong credentials or invalid message')
        # auth_policy = request.registry.auth_policy
        guest = None
        if 'guest' in user:
            guest = user['guest']
        user = {
            'id': user['id'],
            'admin': _is_admin(request, user_id),
            'uid': user['uid'],
            'gid': user['gid'],
            'sgids': user['sgids'],
            'homeDirectory': user['homeDirectory'],
            'email': user['email'],
            'apikey': user['credentials']['apikey'],

        }
        if guest:
            user['guest'] = guest

        secret = request.registry.god_config['secret_passphrase']
        token = jwt.encode({'user': user,
                            # 'exp': datetime.datetime.utcnow() + datetime.timedelta(seconds=3600),
                            'aud': 'urn:godocker/api'}, secret)
    except Exception as e:
        logging.error(str(e))
        return HTTPUnauthorized('Wrong credentials or invalid message')
    return {'token': token}


@view_config(route_name='api_archive', renderer='json', request_method='DELETE')
def api_archive_delete(request):
    '''
    .. http:delete:: /api/1.0/archive/(int:days)?tag=(string:tag)

       Delete all tasks older than *days*, optionally matching tag

       :>json dict: Task
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
       :statuscode 404: not found

    '''
    prometheus_api_inc(request, 'task_archive')
    user = is_authenticated(request, mode="task")
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
    date_filter = datetime.datetime.now() - timedelta(days=int(request.matchdict['days']))
    over_filter = time.mktime(date_filter.timetuple())
    task_filter = {'user.id': user['id'], 'status.date_over': {'$lt': over_filter}}
    if 'tag' in request.params and request.params['tag']:
        task_filter['meta.tags'] = {'$in': [request.params['tag']]}

    tasks = request.registry.db_mongo['jobsover'].find(task_filter)
    if not tasks:
        return HTTPNotFound()
    request.registry.db_mongo['jobsover'].update(task_filter, {'$set': {'status.primary': godutils.STATUS_PENDING_DELETE}})

    to_archive = []
    for task in tasks:
        logging.debug("Archive:Job:" + str(task['id']))
        del task['_id']
        cfg = request.registry.god_config
        task['status']['primary'] = godutils.STATUS_PENDING_DELETE
        redis_call(request, request.registry.db_redis.rpush, cfg['redis_prefix'] + ':jobs:archive', dumps(task))
        to_archive.append(task['id'])
    return {'archive': to_archive}


@view_config(route_name='api_archive', renderer='json', request_method='GET')
def api_archive(request):
    '''
    .. http:get:: /api/1.0/archive/(int:days)

       Archive all tasks older than *days*

       :>json dict: Task
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
       :statuscode 404: not found

    '''
    prometheus_api_inc(request, 'task_archive')
    user = is_authenticated(request, mode="task")
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
    date_filter = datetime.datetime.now() - timedelta(days=int(request.matchdict['days']))
    over_filter = time.mktime(date_filter.timetuple())
    task_filter = {'user.id': user['id'], 'status.date_over': {'$lt': over_filter}}

    tasks = request.registry.db_mongo['jobsover'].find(task_filter)
    if not tasks:
        return HTTPNotFound()

    to_archive = []

    for task in tasks:
        if task['status']['primary'] != godutils.STATUS_ARCHIVED:
            logging.debug("Archive:Job:" + str(task['id']))
            del task['_id']
            cfg = request.registry.god_config
            redis_call(request, request.registry.db_redis.rpush, cfg['redis_prefix'] + ':jobs:archive', dumps(task))
            to_archive.append(task['id'])
    return {'archive': to_archive}


@view_config(route_name='api_task_archive', renderer='json', request_method='DELETE')
def api_task_archive_delete(request):
    '''
    .. http:delete:: /api/1.0/task/(int:task)/archive

       Deletes a task over

       :>json dict: Task
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
       :statuscode 404: not found

    '''
    prometheus_api_inc(request, 'task_archive')
    user = is_authenticated(request, mode="task")
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
    task_filter = {'id': int(request.matchdict['task'])}
    task = request.registry.db_mongo['jobsover'].find_one(task_filter)
    if not task:
        return HTTPNotFound()

    if not _is_user_task_owner(request, user['id'], task):
        return HTTPForbidden('User not member of task project')

    task['status']['primary'] = godutils.STATUS_PENDING_DELETE
    request.registry.db_mongo['jobsover'].update({'_id': task['_id']}, {'$set': {'status.primary': godutils.STATUS_PENDING_DELETE}})
    logging.debug("Archive:Job:" + str(task['id']))
    del task['_id']
    cfg = request.registry.god_config
    redis_call(request, request.registry.db_redis.rpush, cfg['redis_prefix'] + ':jobs:archive', dumps(task))

    if 'credentials' in task['user']:
        del task['user']['credentials']
    return task


@view_config(route_name='api_task_archive', renderer='json', request_method='GET')
def api_task_archive(request):
    '''
    .. http:get:: /api/1.0/task/(int:task)/archive

       Archive a task over

       :>json dict: Task
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
       :statuscode 404: not found

    '''
    prometheus_api_inc(request, 'task_archive')
    user = is_authenticated(request, mode="task")
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
    task_filter = {'id': int(request.matchdict['task'])}
    task = request.registry.db_mongo['jobsover'].find_one(task_filter)
    if not task:
        return HTTPNotFound()

    if not _is_user_task_owner(request, user['id'], task):
        return HTTPForbidden('User not member of task project')

    request.registry.db_mongo['jobsover'].update({'_id': task['_id']}, {'$set': {'status.primary': 'archiving'}})
    logging.debug("Archive:Job:" + str(task['id']))
    del task['_id']
    cfg = request.registry.god_config
    redis_call(request, request.registry.db_redis.rpush, cfg['redis_prefix'] + ':jobs:archive', dumps(task))

    if 'credentials' in task['user']:
        del task['user']['credentials']
    return task


@view_config(route_name='api_task_resume', renderer='json', request_method='GET')
def api_task_resume(request):
    '''
    .. http:get:: /api/1.0/task/(int:task)/resume

       Resume a suspended task

       :>json dict: Task
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
       :statuscode 404: not found

    '''
    prometheus_api_inc(request, 'task_resume')
    user = is_authenticated(request, mode="task")
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
    task_filter = {'id': int(request.matchdict['task'])}
    task = request.registry.db_mongo['jobs'].find_one(task_filter)
    if not task:
        return HTTPNotFound()

    if not _is_user_task_owner(request, user['id'], task):
        return HTTPForbidden('User not member of task project')

    # If kill already requested or job over, do nothing
    if task['status']['secondary'] != godutils.STATUS_SECONDARY_SUSPENDED:
        return task

    task['status']['secondary'] = godutils.STATUS_SECONDARY_RESUME_REQUESTED

    request.registry.db_mongo['jobs'].update({'id': task['id']},
                {'$set': {
                        'status.secondary': task['status']['secondary']
                        }
            })
    cfg = request.registry.god_config
    redis_call(request, request.registry.db_redis.rpush, cfg['redis_prefix'] + ':jobs:resume', dumps(task))
    if 'credentials' in task['user']:
        del task['user']['credentials']
    return task


@view_config(route_name='api_task_monitor', renderer='json', request_method='GET')
def api_task_monitor(request):
    '''
    .. http:get:: /api/1.0/task/(int:container)/monitor

       Get monitoring info for container

       :>json dict: Task
       :statuscode 200: no error
       :statuscode 404: not found

    '''
    prometheus_api_inc(request, 'task_monitor')
    cfg = request.registry.god_config
    task = request.registry.db_mongo['jobs'].find_one({'id': int(request.matchdict['task'])})
    if not task:
        return HTTPNotFound()
    http = urllib3.PoolManager()
    # r = http.request_encode_body('POST', 'http://'+task['container']['meta']['Node']['Name']+':'+str(cfg['cadvisor_port'])+'/api/v1.2/docker/'+task['container']['id'], fields={"num_stats":60,"num_samples":0},encode_multipart=False)
    try:
        r = http.urlopen('POST', 'http://' + task['container']['meta']['Node']['Name'] + ':' + str(cfg['cadvisor_port']) + '/api/v1.2' + cfg['cadvisor_url_part'] + '/' + task['container']['id'], body=json.dumps({"num_stats": cfg['cadvisor_samples'], "num_samples": 0}), headers={'Content-Type': 'application/x-www-form-urlencoded'})
    except Exception:
        return Response('could not get the container', status_code=500)
    if r.status != 200:
        return Response('could not get the container', status_code=r.status)

    res = None
    if sys.version_info >= (3,):
        res = json.loads(r.data.decode())
    else:
        res = json.loads(r.data)
    for key in res:
        if key.startswith('/docker') and key != '/docker/' + task['container']['id']:
            task['container']['id'] = res[key]['id']

    res['id'] = task['container']['id']
    return res


@view_config(route_name='api_task_pending', renderer='json', request_method='GET')
def api_task_pending(request):
    '''
    .. http:get:: /api/1.0/task/(int:task)/pending

       Switch a task in CREATED status to PENDING

       :>json dict: Task
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
       :statuscode 404: not found

    '''
    prometheus_api_inc(request, 'task_pending')
    user = is_authenticated(request, mode="task")
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
    task_filter = {'id': int(request.matchdict['task'])}
    task = request.registry.db_mongo['jobs'].find_one(task_filter)
    if not task:
        return HTTPNotFound()

    if not _is_user_task_owner(request, user['id'], task):
        return HTTPForbidden('User not member of task project')

    if task['status']['primary'] != godutils.STATUS_CREATED:
        return HTTPForbidden('Task not in created status')

    task['status']['primary'] = godutils.STATUS_PENDING
    request.registry.db_mongo['jobs'].update({'id': task['id']},
                {'$set': {
                        'status.primary': task['status']['primary']
                        }
            })
    if 'credentials' in task['user']:
        del task['user']['credentials']
    return task


@view_config(route_name='api_task_reschedule', renderer='json', request_method='GET')
def api_task_reschedule(request):
    '''
    .. http:get:: /api/1.0/task/(int:task)/reschedule

       Reschedule a running task

       :>json dict: Task
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
       :statuscode 404: not found
    '''
    prometheus_api_inc(request, 'task_reschedule')
    user = is_authenticated(request, mode="task")
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
    task_filter = {'id': int(request.matchdict['task'])}
    task = request.registry.db_mongo['jobs'].find_one(task_filter)
    if not task:
        return HTTPNotFound()

    if not _is_user_task_owner(request, user['id'], task):
        return HTTPForbidden('User not member of task project')

    if is_array_task(task) or is_array_child_task(task):
        return HTTPForbidden()
    # If kill already requested or job over, do nothing
    if task['status']['primary'] != godutils.STATUS_RUNNING:
        return task

    task['status']['secondary'] = godutils.STATUS_SECONDARY_RESCHEDULE_REQUESTED
    request.registry.db_mongo['jobs'].update({'id': task['id']}, {'$set': {
                        'status.secondary': godutils.STATUS_SECONDARY_RESCHEDULE_REQUESTED
    }})

    cfg = request.registry.god_config
    redis_call(request, request.registry.db_redis.rpush, cfg['redis_prefix'] + ':jobs:reschedule', dumps(task))
    if 'credentials' in task['user']:
        del task['user']['credentials']
    return task


@view_config(route_name='api_task_suspend', renderer='json', request_method='GET')
def api_task_suspend(request):
    '''
    .. http:get:: /api/1.0/task/(int:task)/suspend

       Suspend a running task

       :>json dict: Task
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
       :statuscode 404: not found
    '''
    prometheus_api_inc(request, 'task_suspend')
    user = is_authenticated(request, mode="task")
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
    task_filter = {'id': int(request.matchdict['task'])}
    task = request.registry.db_mongo['jobs'].find_one(task_filter)
    if not task:
        return HTTPNotFound()

    if not _is_user_task_owner(request, user['id'], task):
        return HTTPForbidden('User not member of task project')

    # If kill already requested or job over, do nothing
    if task['status']['secondary'] == godutils.STATUS_SECONDARY_SUSPEND_REQUESTED or task['status']['primary'] == godutils.STATUS_OVER or task['status']['primary'] == godutils.STATUS_PENDING:
        return task

    task['status']['secondary'] = godutils.STATUS_SECONDARY_SUSPEND_REQUESTED

    request.registry.db_mongo['jobs'].update({'id': task['id']},
                {'$set': {
                        'status.secondary': task['status']['secondary']
                        }
            })
    cfg = request.registry.god_config
    redis_call(request, request.registry.db_redis.rpush, cfg['redis_prefix'] + ':jobs:suspend', dumps(task))
    if 'credentials' in task['user']:
        del task['user']['credentials']
    return task


@view_config(route_name='api_user_task_delete', renderer='json', request_method='DELETE')
def api_user_task_delete(request):
    '''
    .. http:delete:: /api/1.0/user/(str:id)/task

       Kill all user tasks (self or admin), optionally tasks with input query parameter *tag*=MYTAG

       :>json list: list of task ids
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
    '''
    prometheus_api_inc(request, 'task_delete')
    session_user = is_authenticated(request, mode="task")
    if not session_user:
        return HTTPUnauthorized('Not authenticated or expired')

    filter_tag = None
    try:
        filter_tag = request.params['tag']
    except Exception:
        filter_tag = None

    requested_user = request.matchdict['id']
    if not (_is_admin(request, session_user['id']) or session_user['id'] == requested_user):
        return HTTPUnauthorized('Not authorized to access this resource')

    cfg = request.registry.god_config
    killed_tasks = []
    tasks = request.registry.db_mongo['jobs'].find({'user.id': requested_user})
    for task in tasks:
        if task['status']['primary'] == 'over':
            continue

        if filter_tag is not None:
            if 'tags' not in task['meta'] or not task['meta']['tags'] or filter_tag not in task['meta']['tags']:
                logging.debug('Task do not match filter tag ' + filter_tag + ', continue')
                continue
        task['status']['secondary'] = godutils.STATUS_SECONDARY_KILL_REQUESTED
        request.registry.db_mongo['jobs'].update({'id': task['id']},
                     {'$set': {
                             'status.secondary': task['status']['secondary']
                             }
                 })
        redis_call(request, request.registry.db_redis.rpush, cfg['redis_prefix'] + ':jobs:kill', dumps(task))
        killed_tasks.append(task['id'])

    return killed_tasks


@view_config(route_name='api_task', renderer='json', request_method='PUT')
def api_task(request):
    '''
    .. http:put:: /api/1.0/task/(str:id)

       Update a task, only requirements dynamic fields are supported dynamic_fields in config file.
       Other parameters cannot be modified once task is create

       :>json list: Update object with name/value pair, example {"name" : "maxlifespan", "value": "6d"}
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
    '''
    prometheus_api_inc(request, 'task_update')
    session_user = is_authenticated(request, mode="task")
    if not session_user:
        return HTTPUnauthorized('Not authenticated or expired')

    task_filter = {'id': int(request.matchdict['task'])}
    task = request.registry.db_mongo['jobs'].find_one(task_filter)
    if not task:
        return HTTPNotFound()

    if not _is_user_task_owner(request, session_user['id'], task):
        return HTTPForbidden('User not member of task project')

    cfg = request.registry.god_config
    if 'dynamic_fields' not in cfg:
        return HTTPNotFound()

    body = request.body
    if sys.version_info >= (3,):
        body = request.body.decode()
    update_field = json.loads(body, encoding=request.charset)
    is_admin = _is_admin(request, session_user['id'])
    found = False
    for dynamic_field in cfg['dynamic_fields']:
        if update_field['name'] == dynamic_field['name']:
            if dynamic_field['admin_only'] is True and is_admin:
                request.registry.db_mongo['jobs'].update({'id': task['id']}, {'$set': {'requirements.' + update_field['name']: update_field['value']}})
            elif dynamic_field['admin_only'] is False and (is_admin or task['user']['id'] == session_user['id']):
                request.registry.db_mongo['jobs'].update({'id': task['id']}, {'$set': {'requirements.' + update_field['name']: update_field['value']}})
            else:
                return HTTPUnauthorized('Not allowed to modify parameter')
            found = True
            break
    if not found:
        return HTTPNotFound('Field not found or allowed')
    if 'credentials' in task['user']:
        del task['user']['credentials']
    return task


@view_config(route_name='api_task', renderer='json', request_method='DELETE')
def api_task_kill(request):
    '''
    .. http:delete:: /api/1.0/task/(int:task)

       Kill a running task

       :>json dict: Task
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
       :statuscode 404: not found

    '''
    prometheus_api_inc(request, 'task_kill')
    user = is_authenticated(request, mode="task")
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
    task_filter = {'id': int(request.matchdict['task'])}
    task = request.registry.db_mongo['jobs'].find_one(task_filter)
    if not task:
        return HTTPNotFound()

    if not _is_user_task_owner(request, user['id'], task):
        return HTTPForbidden('User not member of task project')

    # If kill already requested or job over, do nothing
    if task['status']['primary'] == 'over':
        return task

    task['status']['secondary'] = godutils.STATUS_SECONDARY_KILL_REQUESTED

    request.registry.db_mongo['jobs'].update({'id': task['id']},
                {'$set': {
                        'status.secondary': task['status']['secondary']
                        }
            })
    cfg = request.registry.god_config
    redis_call(request, request.registry.db_redis.rpush, cfg['redis_prefix'] + ':jobs:kill', dumps(task))
    if 'credentials' in task['user']:
        del task['user']['credentials']
    return task


@view_config(route_name='api_task', renderer='json', request_method='GET')
def api_task_get(request):
    '''
    .. http:get:: /api/1.0/task/(int:task)

       Get a task X-GODOCKER-SHARE in headers to access via a share token.

       Accept

       :>json dict: Task
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
       :statuscode 404: not found

    '''
    prometheus_api_inc(request, 'task_get')
    shared_task = False
    share_token = None
    if 'share' in request.params:
        share_token = request.params['share']
    if 'X-GODOCKER-SHARE' in request.headers:
        share_token = request.headers['X-GODOCKER-SHARE']
    if share_token:
        secret = request.registry.god_config['secret_passphrase']
        try:
            task_id = int(request.matchdict['task'])
            task_info = jwt.decode(share_token, secret, audience='urn:godocker/api')
            if task_info['task_id'] != task_id:
                return HTTPUnauthorized('Not authorized')
            shared_task = True
        except Exception:
            prometheus_api_inc(request, 'auth_error')
            return HTTPUnauthorized('Not authorized')

    user = None
    if not shared_task:
        user = is_authenticated(request, mode="task")

        if not user:
            return HTTPUnauthorized('Not authenticated or expired')

    task_filter = {'id': int(request.matchdict['task'])}
    task = request.registry.db_mongo['jobs'].find_one(task_filter)
    if not task:
        task = request.registry.db_mongo['jobsover'].find_one(task_filter)
    if not task:
        return HTTPNotFound()

    if not shared_task and not _is_user_task_owner(request, user['id'], task):
        return HTTPForbidden('User not member of task project')

    if 'credentials' in task['user']:
        del task['user']['credentials']

    # If task directory is not yet defined (not yet scheduled), add job directory path info
    god_dir_path_defined = False
    for vol in task['container']['volumes']:
        if vol['name'] == 'go-docker':
            god_dir_path_defined = True
            break
    if not god_dir_path_defined:
        task['container']['volumes'].append({'name': 'go-docker', 'path': request.registry.store.get_task_dir(task)})
    return task


@view_config(route_name='api_task_status', renderer='json', request_method='GET')
def api_task_status(request):
    '''
    .. http:get:: /api/1.0/task/(int:task)/status

       Get a task status

       :>json dict: Task
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
       :statuscode 404: not found

    '''
    user = is_authenticated(request, mode="task")
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
    task_filter = {'id': int(request.matchdict['task'])}
    task = request.registry.db_mongo['jobs'].find_one(task_filter)
    if not task:
        task = request.registry.db_mongo['jobsover'].find_one(task_filter)
    if not task:
        return HTTPNotFound()

    if not _is_user_task_owner(request, user['id'], task):
        return HTTPForbidden('User not member of task project')

    return {'id': task['id'], 'status': task['status']}


def validate_task(task):
    schema = {
        "type": "object",
        "properties":
        {
            'meta': {
                "type": "object",
                "properties": {
                    'name': {"type": "string"},
                    'description': {"type": "string"},
                    'tags': {"items": {"type": "string"}}
                 }
            },
            'requirements': {
                'type': 'object',
                'properties': {
                    'cpu': {"type": "number"},
                    'ram': {"type": "number"},
                    'failure_policy': {"type": "number"}
                }
            },
            'container': {
                'type': 'object',
                'properties': {
                    'image': {"type": "string"},
                    'volumes': {
                        "items": {
                            "type": "object",
                            "properties": {
                                'name': {"type": "string"},
                                'acl': {"type": "string"}
                            }
                        }},
                    'network': {"type": "boolean"},
                    'meta': {"type": "null"},  # Contains meta information provided by executor (hostname, ...)
                    'root': {"type": "boolean"}
               }
            },
            'command': {
                'type': 'object',
                'properties': {
                    'interactive': {"type": "boolean"},
                    'cmd': {"type": "string"}
                }
            },
            'status': {
                'type': 'object',
                'properties': {
                    'primary': {"type": ["string", "null"]},
                    'secondary': {"type": ["string", "null"]}
                }
            }
        },
        "required": ["meta", "requirements", "command", "container"]
    }

    validate(task, schema)


@view_config(route_name='api_tasks', renderer='json', request_method='POST')
def api_task_add(request):
    '''
    .. http:post:: /api/1.0/task

       Create a new task for user

       :<json dict body: JSON task to add
       :>json dict: {'msg': 'Task added', 'id': task_id}
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 503: service unavailable
    '''
    if _in_maintenance(request):
        return HTTPServiceUnavailable

    prometheus_api_inc(request, 'task_add')
    user = is_authenticated(request, mode="task")
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')

    cfg = request.registry.god_config

    body = request.body
    if sys.version_info >= (3,):
        body = request.body.decode()
    task = json.loads(body, encoding=request.charset)
    try:
        validate_task(task)
    except Exception as e:
        logging.error(str(e))
        return HTTPBadRequest('Wrong task format')

    if 'status' not in task:
        task['status'] = {'primary': None, 'secondary': None, 'exitcode': None}
    if not task['status']['primary']:
        task['status']['primary'] = godutils.STATUS_PENDING
        task['status']['secondary'] = None
        task['status']['reason'] = None
        task['status']['exitcode'] = None
    else:
        if task['status']['primary'] not in [godutils.STATUS_CREATED, godutils.STATUS_PENDING]:
            return HTTPBadRequest('Invalid status for new task: ' + str(task['status']['primary']))
        else:
            task['status']['secondary'] = None
            task['status']['reason'] = None
            task['status']['exitcode'] = None

    if 'notify' not in task:
        task['notify'] = {'email': False}

    if 'network' not in task['requirements'] and request.registry.network_plugin is not None:
        task['requirements']['network'] = request.registry.network_plugin.default_network()

    if task['container']['image'] is None or not task['container']['image']:
        for vol in cfg['default_images']:
            if 'default' in vol and vol['default']:
                task['container']['image'] = vol['url']
                break
        if task['container']['image'] is None:
            return HTTPBadRequest('No image specified and no default image available')

    task['container']['id'] = None

    if 'link_image_to_registry' in cfg:
        image_url = task['container']['image']
        if ':' in image_url:
            image_url_parts = image_url.split(':')
            image_url = image_url_parts[0]

        if cfg['use_private_registry']:
            if not task['container']['image'].startswith(cfg['use_private_registry']):
                image_url = cfg['use_private_registry'] + '/' + task['container']['image']
        image_url_parts = image_url.split('/')
        if '.' in image_url_parts[0]:
            image_url_parts.pop(0)
            image_name = '/'.join(image_url_parts)
            for registry in cfg['link_image_to_registry']:
                if image_url.startswith(registry['name']):
                    image_url = registry['url'].replace('$IMAGE', image_name)
                    break

        else:
            image_url_parts = image_url.split('/')
            if len(image_url_parts) == 1:
                image_url = 'https://hub.docker.com/_/' + image_url + '/'
            else:
                image_url = 'https://hub.docker.com/r/' + image_url + '/'

        task['container']['image_url'] = image_url

    if not cfg['allow_user_images']:
        allowed = False
        for vol in cfg['default_images']:
            image_name = task['container']['image'].split(':')[0]
            if image_name == vol['name']:
                allowed = True
                break
        if not allowed:
            return HTTPBadRequest('Requested image is not allowed')

    dt = datetime.datetime.now()
    task['date'] = time.mktime(dt.timetuple())
    task['status']['date_over'] = None

    auth_policy = request.registry.auth_policy

    if 'user' not in task:
        task['user'] = {}

    if not task['command']['cmd'].startswith("#!"):
        task['command']['cmd'] = "#!/bin/bash\n" + task['command']['cmd']

    if _is_admin(request, user['id']) and 'id' in task['user']:
        user_from_db = _get_user(request, task['user']['id'])
    else:
        user_from_db = _get_user(request, user['id'])

    if 'guest' in user_from_db and user_from_db['guest']:
        if not user_from_db['guest']['active']:
            return HTTPUnauthorized('Your guest status is not active')
        else:
            logging.debug("Guest access")
            task['user']['guest'] = True
        if not cfg['guest_allow_root']:
            task['container']['root'] = False
        if cfg['guest_home_root'] is None:
            # Remove home if requested
            index = -1
            counter = 0
            for volume in task['container']['volumes']:
                if volume['name'] == 'home':
                    index = counter
                    break
                counter += 1
            if index > -1:
                task['container']['volumes'].pop(index)
    if _is_admin(request, user['id']):
        if 'id' not in task['user']:
            task['user']['id'] = user['id']
    else:
        task['user']['id'] = user['id']

    task['user']['uid'] = user_from_db['uid']
    task['user']['gid'] = user_from_db['gid']
    task['user']['sgids'] = user_from_db['sgids']

    if 'email' not in task['user'] or task['user']['email'] is None:
        if 'email' not in user_from_db:
            task['user']['email'] = None
        else:
            task['user']['email'] = user_from_db['email']

    task['user']['credentials'] = {
        'apikey': user_from_db['credentials']['apikey'],
        'public': user_from_db['credentials']['public']
    }

    if 'usage' not in user_from_db:
        user_from_db['usage'] = {}

    if 'rate_limit' in cfg and cfg['rate_limit'] is not None:
        current_rate = redis_call(request, request.registry.db_redis.get, cfg['redis_prefix'] +
                                   ':user:' + str(task['user']['id']) + ':rate')
        if current_rate is not None and int(current_rate) >= cfg['rate_limit']:
            return HTTPForbidden('User rate limit reached: ' +
                                 str(cfg['rate_limit']))

    if 'rate_limit_all' in cfg and cfg['rate_limit_all'] is not None:
        current_rate = redis_call(request, request.registry.db_redis.get, cfg['redis_prefix'] + ':jobs:queued')
        if current_rate is not None and int(current_rate) >= cfg['rate_limit_all']:
            return HTTPForbidden('Global rate limit reached: ' + str(cfg['rate_limit_all']) + ', please retry later')

    if 'quota_time' in user_from_db['usage']:
        task['requirements']['user_quota_time'] = user_from_db['usage']['quota_time']
    else:
        task['requirements']['user_quota_time'] = 0

    if 'quota_cpu' in user_from_db['usage']:
        task['requirements']['user_quota_cpu'] = user_from_db['usage']['quota_cpu']
    else:
        task['requirements']['user_quota_cpu'] = 0

    if 'quota_ram' in user_from_db['usage']:
        task['requirements']['user_quota_ram'] = user_from_db['usage']['quota_ram']
    else:
        task['requirements']['user_quota_ram'] = 0

    if 'quota_gpu' in user_from_db['usage']:
        task['requirements']['user_quota_gpu'] = user_from_db['usage']['quota_gpu']
    else:
        task['requirements']['user_quota_gpu'] = 0

    if 'tmpstorage' not in task['requirements']:
        task['requirements']['tmpstorage'] = None
    else:
        if task['requirements']['tmpstorage'] is not None:
            task['requirements']['tmpstorage']['path'] = None

    if 'project' not in task['user']:
        task['user']['project'] = 'default'

    if not cfg['projects']['is_default_allowed'] and task['user']['project'] == 'default':
        return HTTPForbidden('default project is not allowed, a project must be selected')

    project = None
    if task['user']['project'] != 'default':
        project = request.registry.db_mongo['projects'].find_one({'id': task['user']['project']})
        if not project:
            return HTTPForbidden('Project not valid')
        is_project_member = False

        if task['user']['id'] in project['members']:
            is_project_member = True

        if not is_project_member and str(task['user']['gid']) in project['members']:
            is_project_member = True

        if not is_project_member:
            # test sgids
            for sgid in task['user']['sgids']:
                if str(sgid) in project['members']:
                    is_project_member = True
                    break

        if not is_project_member:
            return HTTPForbidden('Not a project member')

        task['requirements']['project_quota_time'] = project['quota_time']
        task['requirements']['project_quota_cpu'] = project['quota_cpu']
        task['requirements']['project_quota_ram'] = project['quota_ram']
        # check quota_gpu defined in project for old projects created before gpu quotas
        if 'quota_gpu' in project:
            task['requirements']['project_quota_gpu'] = project['quota_gpu']
        else:
            task['requirements']['project_quota_gpu'] = 0
    else:
        task['requirements']['project_quota_time'] = 0
        task['requirements']['project_quota_cpu'] = 0
        task['requirements']['project_quota_ram'] = 0
        task['requirements']['project_quota_gpu'] = 0

    if 'root' not in task['container']:
        task['container']['root'] = False

    volumes = auth_policy.get_volumes(user_from_db, task['container']['volumes'], task['container']['root'])

    if project is not None:
        for project_volume in project['volumes']:
            # Add project volumes
            if task['container']['root']:
                project_volume['acl'] = 'ro'
            volumes.append(project_volume)

    task['container']['volumes'] = volumes

    task['parent_task_id'] = None

    if 'ports' in task['requirements']:
        try:
            for port in task['requirements']['ports']:
                port = int(port)
        except Exception as e:
            return HTTPBadRequest('Invalid ports')

    # Add subsystem info in tags
    task['meta']['executor'] = []
    if 'executor' in task['requirements'] and task['requirements']['executor']:
        subsystem = task['requirements']['executor'].split(':')
        if len(subsystem) > 1 and subsystem[0] not in task['meta']['executor']:
            task['meta']['executor'].append(subsystem[0])
            if len(subsystem) > 1:
                task['meta']['executor'].append('queue:' + subsystem[1])
    else:
        task['requirements']['executor'] = cfg['executors'][0]

    if 'array' in task['requirements']:
        task['requirements']['array']['task_id'] = None
        task['requirements']['array']['nb_tasks'] = None
        task['requirements']['array']['nb_tasks_over'] = None
        task['requirements']['array']['tasks'] = []
    else:
        task['requirements']['array'] = {
            'task_id': None,
            'nb_tasks': None,
            'nb_tasks_over': None,
            'tasks': []
        }

    if 'cpu' not in task['requirements']:
        task['requirements']['cpu'] = cfg['defaults_cpu']
    if 'ram' not in task['requirements']:
        task['requirements']['ram'] = cfg['defaults_ram']
    if 'gpus' not in task['requirements']:
        task['requirements']['gpus'] = 0

    if 'hooks' in task:
        if not isinstance(task['hooks'], dict):
            return HTTPBadRequest('Hooks parameter must be a dict')
        if not task['hooks'].get('ok', None):
            return HTTPBadRequest('Hooks parameter is missing the *ok* key')
        if not task['hooks'].get('ko', None):
            # If *ko* not set, use same as ok
            task['hooks']['ko'] = task['hooks']['ok']

    task_id = _add_task(request, task)

    return {'msg': 'Task added', 'id': task_id}


def _add_task(request, task):
    '''
    Add a new task, if status not set, set it to pending

    Automatically atribute an id to the task
    :param task: Task to insert
    :type task: dict
    :return: task id
    '''
    cfg = request.registry.god_config
    task_id = request.registry.db_redis.incr(cfg['redis_prefix'] + ':jobs')
    redis_call(request, request.registry.db_redis.incr, cfg['redis_prefix'] + ':jobs:queued')
    redis_call(request, request.registry.db_redis.incr, cfg['redis_prefix'] + ':user:' + str(task['user']['id']) + ':rate')
    task['id'] = task_id
    if not task['status']['primary']:
        task['status']['primary'] = godutils.STATUS_PENDING

    if not cfg['allow_root']:
        task['container']['root'] = False

    if 'array' not in task['requirements']:
        task['requirements']['array'] = {'values': None}

    if 'tasks' not in task['requirements']:
        task['requirements']['tasks'] = []

    if is_array_task(task):
        task['requirements']['array']['nb_tasks'] = 0
        task['requirements']['array']['nb_tasks_over'] = 0
        task['requirements']['array']['tasks'] = []
        array_req = task['requirements']['array']['values'].split(':')
        array_first = 0
        array_last = 0
        array_step = 1
        if len(array_req) == 1:
            array_first = 1
            array_last = int(array_req[0])
            array_step = 1
        else:
            array_first = int(array_req[0])
            array_last = int(array_req[1])
            if len(array_req) == 3:
                array_step = int(array_req[2])
        for i in range(array_first, array_last + array_step, array_step):
            subtask = deepcopy(task)
            subtask['requirements']['array']['nb_tasks'] = 0
            subtask['requirements']['array']['tasks'] = []
            subtask['requirements']['array']['task_id'] = i
            subtask['parent_task_id'] = task['id']
            subtask['requirements']['array']['values'] = None
            subtask_id = _add_task(request, subtask)
            task['requirements']['array']['nb_tasks'] += 1
            task['requirements']['array']['tasks'].append(subtask_id)
        redis_call(request, request.registry.db_redis.set, cfg['redis_prefix'] + ':job:' + str(task['id']) + ':subtask', task['requirements']['array']['nb_tasks'])

    request.registry.db_mongo['jobs'].insert(task)
    return task_id


@view_config(route_name='api_images', renderer='json', request_method='GET')
def api_images(request):
    '''
    .. http:get:: /api/1.0/image

        List used images

       :>json dict: List of used images
       :statuscode 200: no error
    '''
    cfg = request.registry.god_config
    return redis_call(request, request.registry.db_redis.hkeys, cfg['redis_prefix'] + ':images')


@view_config(route_name='api_image_count', renderer='json', request_method='GET')
def api_image_count(request):
    '''
    .. http:get:: /api/1.0/image/(str:image)

        List used images

       :>json dict: List of used images
       :statuscode 200: no error
    '''
    cfg = request.registry.god_config
    image = request.matchdict['image']
    return {'image': image, 'count': redis_call(request, request.registry.db_redis.hget, cfg['redis_prefix'] + ':images', image)}


@view_config(route_name='api_task_active', renderer='json', request_method='GET')
def api_task_active(request):
    '''
    .. http:get:: /api/1.0/task/active

        List running or pending tasks for user

       :>json dict: List of tasks
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
    '''
    prometheus_api_inc(request, 'task_list_active')
    user = is_authenticated(request)
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')

    task_filter = {}
    task_filter['user.id'] = user['id']
    if 'all' in request.params and request.params['all'] and request.params['all'] == 'true':
        if not _is_admin(request, user['id']):
            return HTTPForbidden('Not authorized to access this resource')
        else:
            del task_filter['user.id']

    limit = 0
    try:
        limit = int(request.params['limit'])
    except Exception:
        limit = 0

    project = None
    try:
        project = request.params['project']
        if project == 'my':
            project = None
    except Exception:
        project = None

    if project is not None:
        if project != 'default' and _user_in_project(request, user['id'], project):
            task_filter = {'user.project': project}
        else:
            task_filter['user.project'] = project

    if 'draw' in request.params:
        start = 0
        if 'start' in request.params:
            start = int(request.params['start'])
        limit = 10
        if 'limit' in request.params:
            limit = int(request.params['limit'])
        reverse = DESCENDING
        if 'reverse' in request.params and request.params['reverse'] in ['true', 'True', '1']:
            reverse = ASCENDING

        try:
            if 'start_date' in request.params and 'end_date' in request.params:
                if request.params['start_date'] and request.params['end_date']:
                    task_filter['date'] = {'$gte': int(request.params['start_date']), '$lte': int(request.params['end_date'])}
            else:
                if 'start_date' in request.params and request.params['start_date']:
                    task_filter['date'] = {'$gte': int(request.params['start_date'])}
                if 'end_date' in request.params and request.params['end_date']:
                    task_filter['date'] = {'$lte': int(request.params['end_date'])}
        except Exception as e:
            logging.info("Error in date filter: " + str(e))

        orderBy = 'id'
        if 'orderBy' in request.params:
            orderBy = request.params['orderBy']
            if orderBy == 'name':
                orderBy = 'meta.name'
            elif orderBy == 'container_image':
                orderBy = 'container.image'
            elif orderBy == 'user_id':
                orderBy == 'user.id'
            elif orderBy == 'status':
                orderBy = 'status.primary'
            elif orderBy == 'project':
                orderBy = 'user.project'
            elif orderBy == 'container':
                orderBy = 'container.image'

        if 'regex' in request.params and request.params['regex'] not in ['', 'null']:
            regex = request.params['regex']
            task_filter['$or'] = [
                {'meta.name': {'$regex': regex, '$options': 'i'}},
                {'meta.tags': {'$regex': regex, '$options': 'i'}},
                {'user.id': {'$regex': regex, '$options': 'i'}},
                {'container.image': {'$regex': regex, '$options': 'i'}},
            ]

        nb_tasks = tasks = request.registry.db_mongo['jobs'].find(task_filter).count()
        tasks = request.registry.db_mongo['jobs'].find(task_filter, sort=[(orderBy, reverse)], skip=start, limit=limit)
        res = [{
            'recordsTotal': nb_tasks,
            'data': []
            }
        ]
        for task in tasks:
            if 'credentials' in task['user']:
                del task['user']['credentials']
            res[0]['data'].append(task)
        return res

    tasks = request.registry.db_mongo['jobs'].find(task_filter, sort=[('id', DESCENDING)])
    res = []
    i = 0
    for task in tasks:
        if 'credentials' in task['user']:
            del task['user']['credentials']
        if limit > 0:
            if i < limit:
                res.append(task)
        else:
            res.append(task)
        i += 1
    return res


@view_config(route_name='api_task_active_all', renderer='json', request_method='GET')
def api_task_active_all(request):
    '''
    .. http:get:: /api/1.0/task/active/all

        List running or pending tasks for all users
        Restricted to administrators

       :>json dict: List of tasks
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
    '''
    user = is_authenticated(request)
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
    if not _is_admin(request, user['id']):
        return HTTPForbidden('Not authorized to access this resource')

    task_filter = {}

    tasks = request.registry.db_mongo['jobs'].find(task_filter)
    res = []
    for task in tasks:
        if 'credentials' in task['user']:
            del task['user']['credentials']
        res.append(task)
    return res


@view_config(route_name='api_task_over', renderer='json', request_method='GET')
def api_task_over(request):
    '''
    .. http:get:: /api/1.0/task/over

        List last 100 finished tasks, or the last X tasks, if *limit* request parameter is set

        Support for pagination with following optional request parameters:
            draw: set this to manage pagination and filtering
            start_date: timestamp to limit search in time
            end_date: timestamp to limit search in time
            start: number of tasks to skip
            limit: number of results to return
            reverse: set to *true* to reverse the results
            orderBy: field used to sort the results [id, name, container_image, user_id]
            regex: text to use to filter the results container regex value (case insensitive)

        If pagination is enabled with *draw* parameter, result is an array of 1 element with fields:
            data: list of tasks matching the conditions
            recordsTotal: total number of records


       :>json list: List of tasks
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
    '''
    prometheus_api_inc(request, 'task_list_over')
    user = is_authenticated(request)
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')

    task_filter = {}
    task_filter['user.id'] = user['id']
    if 'all' in request.params and request.params['all'] and request.params['all'] == 'true':
        if not _is_admin(request, user['id']):
            return HTTPForbidden('Not authorized to access this resource')
        else:
            del task_filter['user.id']

    project = None
    try:
        project = request.params['project']
        if project == 'my':
            project = None
    except Exception:
        project = None

    if project is not None:
        if project != 'default' and _user_in_project(request, user['id'], project):
            task_filter = {'user.project': project}
        else:
            task_filter['user.project'] = project

    if 'draw' in request.params:
        start = 0
        if 'start' in request.params:
            start = int(request.params['start'])
        limit = 10
        if 'limit' in request.params:
            limit = int(request.params['limit'])
        reverse = DESCENDING
        if 'reverse' in request.params and request.params['reverse'] in ['true', 'True', '1']:
            reverse = ASCENDING

        try:
            if 'start_date' in request.params and 'end_date' in request.params:
                if request.params['start_date'] and request.params['end_date']:
                    task_filter['date'] = {'$gte': int(request.params['start_date']), '$lte': int(request.params['end_date'])}
            else:
                if 'start_date' in request.params and request.params['start_date']:
                    task_filter['date'] = {'$gte': int(request.params['start_date'])}
                if 'end_date' in request.params and request.params['end_date']:
                    task_filter['date'] = {'$lte': int(request.params['end_date'])}
        except Exception as e:
            logging.info("Error in date filter: " + str(e))

        orderBy = 'id'
        if 'orderBy' in request.params:
            orderBy = request.params['orderBy']
            if orderBy == 'name':
                orderBy = 'meta.name'
            if orderBy == 'container_image':
                orderBy = 'container.image'
            if orderBy == 'user_id':
                orderBy == 'user.id'

        if 'regex' in request.params and request.params['regex'] not in ['', 'null']:
            regex = request.params['regex']
            task_filter['$or'] = [
                {'meta.name': {'$regex': regex, '$options': 'i'}},
                {'meta.tags': {'$regex': regex, '$options': 'i'}},
                {'user.id': {'$regex': regex, '$options': 'i'}},
                {'container.image': {'$regex': regex, '$options': 'i'}},
            ]

        nb_tasks = tasks = request.registry.db_mongo['jobsover'].find(task_filter).count()
        tasks = request.registry.db_mongo['jobsover'].find(task_filter, sort=[(orderBy, reverse)], skip=start, limit=limit)
        res = [{
            'recordsTotal': nb_tasks,
            'data': []
            }
        ]
        for task in tasks:
            if 'credentials' in task['user']:
                del task['user']['credentials']
            res[0]['data'].append(task)
        return res

    limit = 100
    if 'limit' in request.params:
        limit = int(request.params['limit'])

    tasks = request.registry.db_mongo['jobsover'].find(task_filter, sort=[('id', DESCENDING)], limit=limit)
    res = []
    for task in tasks:
        if 'credentials' in task['user']:
            del task['user']['credentials']
        res.append(task)
    return res


@view_config(route_name='api_task_over_all', renderer='json', request_method='GET')
def api_task_over_all(request):
    '''
    .. http:get:: /api/1.0/task/over/all

        List over for all users (last 100)
        Restricted to administrators

       :>json dict: List of tasks
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
    '''
    user = is_authenticated(request)
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
    if not _is_admin(request, user['id']):
        return HTTPForbidden('Not authorized to access this resource')

    task_filter = {}

    tasks = request.registry.db_mongo['jobsover'].find(task_filter, sort=[('id', DESCENDING)], limit=100)
    res = []
    for task in tasks:
        if 'credentials' in task['user']:
            del task['user']['credentials']
        res.append(task)
    return res


@view_config(route_name='api_task_over_cursor', renderer='json')
def api_task_over_cursor(request):
    '''
    .. http:post:: /api/1.0/task/over/(int:skip)/(int:limit)

        Get finished tasks

        Params:
          skip: number of records to skip in search,
          limit: maximum number of records to return

        Json optional filter: {'start': timestamp, 'end': timestamp}

       :<json dict: Optional filter on task start date (timestamp)
       :>json dict: List of tasks
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
    '''
    limit = int(request.matchdict['limit'])
    skip = int(request.matchdict['skip'])
    body = request.body
    if sys.version_info >= (3,):
        body = request.body.decode()
    if body:
        date_filter = json.loads(body, encoding=request.charset)
    else:
        date_filter = {}
    user = is_authenticated(request)
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')

    task_filter = {}
    task_filter['user.id'] = user['id']

    project = None
    try:
        project = request.params['project']
        if project == 'my':
            project = None
    except Exception:
        project = None

    if project is not None:
        if project != 'default' and _user_in_project(request, user['id'], project):
            task_filter = {'user.project': project}
        else:
            task_filter['user.project'] = project

    if 'start' in date_filter and 'end' in date_filter:
        task_filter['date'] = {'$gte': date_filter['start'], '$lte': date_filter['end']}
    else:
        if 'start' in date_filter:
            task_filter['date'] = {'$gte': date_filter['start']}
        if 'end' in date_filter:
            task_filter['date'] = {'$lte': date_filter['end']}

    tasks = request.registry.db_mongo['jobsover'].find(task_filter, sort=[('id', DESCENDING)], skip=skip, limit=limit)
    res = []
    for task in tasks:
        if 'credentials' in task['user']:
            del task['user']['credentials']
        res.append(task)
    return res


@view_config(route_name='api_task_over_all_cursor', renderer='json')
def api_task_over_all_cursor(request):
    '''
    .. http:post:: /api/1.0/task/over/all/(int:skip)/(int:limit)

        Get finished tasks for all users (Administrator only)

        Params:
          skip: number of records to skip in search,
          limit: maximum number of records to return

        Json optional filter: {'start': timestamp, 'end': timestamp}

       :<json dict: Optional filter on task start date (timestamp)
       :>json dict: List of tasks
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
    '''
    limit = int(request.matchdict['limit'])
    skip = int(request.matchdict['skip'])
    body = request.body
    if sys.version_info >= (3,):
        body = request.body.decode()
    if body:
        date_filter = json.loads(body, encoding=request.charset)
    else:
        date_filter = {}
    user = is_authenticated(request)
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
    if not _is_admin(request, user['id']):
        return HTTPForbidden('Not authorized to access this resource')

    task_filter = {}
    if 'start' in date_filter and 'end' in date_filter:
        task_filter['date'] = {'$gte': date_filter['start'], '$lte': date_filter['end']}
    else:
        if 'start' in date_filter:
            task_filter['date'] = {'$gte': date_filter['start']}
        if 'end' in date_filter:
            task_filter['date'] = {'$lte': date_filter['end']}
    tasks = request.registry.db_mongo['jobsover'].find(task_filter, sort=[('id', DESCENDING)], skip=skip, limit=limit)
    res = []
    for task in tasks:
        if 'credentials' in task['user']:
            del task['user']['credentials']
        res.append(task)
    return res


@view_config(route_name='api_count', renderer='json', request_method='GET')
def api_count_running(request):
    '''
    .. http:get:: /api/1.0/task/status/(str:status)/count

        Count number of tasks for all users

        status request parameter should be in [ 'pending', 'running', 'kill', 'all']

       :>json dict: {'total': number of tasks, 'status': requested status}
       :statuscode 200: no error
    '''
    status = request.matchdict['status']
    res = 0
    cfg = request.registry.god_config
    if status == 'running':
        res = redis_call(request, request.registry.db_redis.llen, cfg['redis_prefix'] + ':jobs:running')
    elif status == 'pending':
        res = request.registry.db_mongo['jobs'].find({'status.primary': godutils.STATUS_PENDING}).count()
    elif status == 'kill':
        res = redis_call(request, request.registry.db_redis.llen, cfg['redis_prefix'] + ':jobs:kill')
    elif status == 'all':
        res = redis_call(request, request.registry.db_redis.get, cfg['redis_prefix'] + ':jobs')
    elif status == 'my':
        user = is_authenticated(request)
        if user:
            res = redis_call(request, request.registry.db_redis.get, cfg['redis_prefix'] + ':jobs:stats:user:' + user['id'])
    if res is None:
            res = 0

    return {'total': int(res), 'status': status}


@view_config(route_name='api_ping', renderer='json', request_method='GET')
def api_ping(request):
    '''
    .. http:get:: /api/1.0/ping

       Ping web site

       :>json dict:  {'msg': 'pong'}
       :statuscode 200: no error
    '''
    # cfg = request.registry.god_config
    status_manager = request.registry.status_manager
    if status_manager is not None:
        hostname = socket.gethostbyaddr(socket.gethostname())[0]
        proc_name = 'web-' + hostname
        status_manager.keep_alive(proc_name, 'web')

    return {'msg': 'pong'}


# Marketplace
@view_config(route_name='api_marketplace_recipes', renderer='json', request_method='GET')
def api_marketplace_recipes(request):
    '''
    .. http:get:: /api/1.0/marketplace/recipe

        Get public recipes and user private recipes if logged

       :>json list: {'id': recipe_id, 'description': recipe_description}
       :statuscode 200: no error
    '''
    user = is_authenticated(request)
    recipe_list = []
    recipe_filter = {}
    owner_only = False
    try:
        my_param = request.matchdict['my']
        if int(my_param) == 1:
            owner_only = True
    except Exception:
        owner_only = False
    if owner_only:
        if user:
            recipe_filter = {'owner': user['id']}
        else:
            return HTTPForbidden()
    else:
        if not user:
            recipe_filter = {'visible': True}
        elif not _is_admin(request, user['id']):
            recipe_filter = {'$or': [{'visible': True}, {'owner': user['id']}]}

    recipes = request.registry.db_mongo['recipes'].find(recipe_filter)
    for recipe in recipes:
        recipe_list.append(recipe)
    return recipe_list


# api_marketplace_recipe, GET/POST/PU/DELETE /api/1.0/marketplace/recipe/{str:recipe}
@view_config(route_name='api_marketplace_recipe', renderer='json', request_method='GET')
def api_marketplace_recipe(request):
    '''
    .. http:get:: /api/1.0/marketplace/recipe/{str:recipe}

        Get a public or owned recipe

       :>json dict: {'id': recipe_id, 'description': recipe_description, 'task': task_originating_id}
       :statuscode 200: no error
       :statuscode 403: not authorized
       :statuscode 404: not found
    '''
    recipe_id = request.matchdict['recipe']
    recipe_filter = {'id': recipe_id}
    user = is_authenticated(request)
    recipe = request.registry.db_mongo['recipes'].find_one(recipe_filter)
    if recipe:
        if not recipe['visible']:
            if recipe['owner'] != user['id'] and not _is_admin(request, user['id']):
                return HTTPForbidden()
        if recipe['task']:
            task_filter = {'id': int(recipe['task'])}
            task = request.registry.db_mongo['jobsover'].find_one(task_filter)
            if task:
                if 'status' in task:
                    del task['status']
                if 'container' in task:
                    task['container']['id'] = None
                    task['container']['meta'] = None
                    task['container']['stats'] = None
                recipe['recipe'] = task
        return recipe
    else:
        return HTTPNotFound()


@view_config(route_name='api_marketplace_recipe', renderer='json', request_method='POST')
def api_marketplace_recipe_new(request):
    '''
    .. http:post:: /api/1.0/marketplace/recipe/{str:recipe}

        Create a recipe. Post body must contain description and task elements

       :statuscode 200: no error
       :statuscode 403: not authorized
       :statuscode 404: not found
    '''
    recipe_id = request.matchdict['recipe']
    recipe_filter = {'id': recipe_id}
    user = is_authenticated(request)
    if not user:
        return HTTPForbidden()
    recipe = request.registry.db_mongo['recipes'].find_one(recipe_filter)
    if recipe:
        if recipe['owner'] != user['id']:
            return HTTPForbidden('recipe already exists')

    body = request.body
    if sys.version_info >= (3,):
        body = request.body.decode()

    form = json.loads(body, encoding=request.charset)
    if 'description' not in form or not form['description']:
        return HTTPForbidden('missing description')
    if 'id' not in form or not form['id']:
        return HTTPForbidden('missing name')
    if 'visible' not in form:
        form['visible'] = True
    else:
        if form['visible'] == 'true':
            form['visible'] = True
        else:
            form['visible'] = False

    if 'task' not in form:
        form['task'] = None

    if recipe:
        request.registry.db_mongo['recipes'].update(
            {'id': recipe_id},
            {'$set': {
                'description': form['description'],
                'task': form['task'],
                'owner': user['id'],
                'visible': form['visible']
            }
        })
        return {'msg': 'recipe create', 'id': recipe_id}
    else:
        request.registry.db_mongo['recipes'].insert({
            'id': recipe_id,
            'description': form['description'],
            'task': form['task'],
            'owner': user['id'],
            'visible': form['visible']
        })
        return {'msg': 'recipe create', 'id': recipe_id}


@view_config(route_name='api_marketplace_recipe', renderer='json', request_method='PUT')
def api_marketplace_recipe_edit(request):
    '''
    .. http:put:: /api/1.0/marketplace/recipe/{str:recipe}

        Update an owned recipe. Put body can contain description and/or task elements

       :statuscode 200: no error
       :statuscode 403: not authorized
       :statuscode 404: not found
    '''
    recipe_id = request.matchdict['recipe']
    recipe_filter = {'id': recipe_id}
    user = is_authenticated(request)
    if not user:
        return HTTPForbidden()
    recipe = request.registry.db_mongo['recipes'].find_one(recipe_filter)
    if not recipe:
        return HTTPNotFound()

    if recipe['owner'] != user['id'] and not _is_admin(request, user['id']):
        return HTTPForbidden()

    body = request.body
    if sys.version_info >= (3,):
        body = request.body.decode()

    form = json.loads(body, encoding=request.charset)

    updates = {'owner': user['id']}
    if 'description' in form and form['description']:
        updates['description'] = form['description']
    if 'task' in form and form['task']:
        updates['task'] = form['task']

    request.registry.db_mongo['recipes'].update({'id': recipe_id}, {'$set': updates})
    return {'msg': 'recipe updated', 'id': recipe_id}


@view_config(route_name='api_marketplace_recipe', renderer='json', request_method='DELETE')
def api_marketplace_recipe_delete(request):
    '''
    .. http:delete:: /api/1.0/marketplace/recipe/{str:recipe}

        Delete a owned recipe

       :statuscode 200: no error
       :statuscode 403: not authorized
       :statuscode 404: not found
    '''
    recipe_id = request.matchdict['recipe']
    recipe_filter = {'id': recipe_id}
    user = is_authenticated(request)
    if not user and not _is_admin(request, user['id']):
        return HTTPForbidden()
    recipe = request.registry.db_mongo['recipes'].find_one(recipe_filter)
    if recipe:
        if not recipe['visible']:
            if recipe['owner'] != user['id']:
                return HTTPForbidden()
        request.registry.db_mongo['recipes'].remove(recipe_filter)
        return {'msg': 'recipe deleted'}
    else:
        return HTTPNotFound()


@view_config(route_name='api_app_tokens', renderer='json', request_method='GET')
def api_app_tokens(request):
    '''
    Get applications token
    '''
    session_user = is_authenticated(request)
    if not session_user:
        return HTTPForbidden()
    apps_db = request.registry.db_mongo['remote_app'].find({'user': session_user['id']})
    apps = []
    for app_db in apps_db:
        apps.append(app_db)
    return apps


@view_config(route_name='api_app_tokens', renderer='json', request_method='POST')
def api_app_token_add(request):
    '''
    Generate a new application token
    '''
    session_user = is_authenticated(request)
    if not session_user:
        return HTTPForbidden()
    body = request.body
    if sys.version_info >= (3,):
        body = request.body.decode()

    form = json.loads(body, encoding=request.charset)
    app = form['name']
    app_db = request.registry.db_mongo['remote_app'].find_one({'name': app})
    if app_db:
        return HTTPForbidden()
    token = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(20))
    app_db = {'name': app,
              'token': token,
              'user': session_user['id']
              }
    request.registry.db_mongo['remote_app'].insert(app_db)
    return app_db


@view_config(route_name='api_app_token', renderer='json', request_method='DELETE')
def api_app_token_delete(request):
    '''
    Delete an application token
    '''
    session_user = is_authenticated(request)
    if not session_user:
        return HTTPForbidden()
    app = request.matchdict['id']

    app_db = request.registry.db_mongo['remote_app'].find_one({'_id': ObjectId(app)})
    if not app_db:
        return HTTPNotFound()
    if app_db['user'] != session_user['id'] and not _is_admin(request, session_user['id']):
        return HTTPForbidden()
    request.registry.db_mongo['remote_app'].delete_one({'_id': ObjectId(app)})
    return app_db


@view_config(route_name='api_app_user_token', renderer='json', request_method='GET')
def api_app_user_token(request):
    '''
    Get user info for a token application

    Header must contain a JWT encoded bearer containing same app_token as requested token.
    Bearer must be decoded using shared_secret_passphrase.
    '''
    token = request.matchdict['token']
    if request.authorization is not None:
        try:
            (type, bearer) = request.authorization
            secret = request.registry.god_config['shared_secret_passphrase']
            # If decode ok and not expired
            user = jwt.decode(bearer, secret, audience='urn:godocker/api')
            user_db = None
            if 'app_token' in user['user']:
                if user['user']['app_token'] == token:
                    app_db = request.registry.db_mongo['remote_app'].find_one({'token': token})
                    user_db = request.registry.db_mongo['users'].find_one({'id': app_db['user']})
            if user_db is None:
                return HTTPForbidden()
            else:
                return user_db
        except Exception as e:
            logging.error("Failed to decode header: " + str(e))
            prometheus_api_inc(request, 'auth_error')
            return None
    else:
        return HTTPForbidden()


def deny(msg):
    return {"Allow": False, "Msg": msg, "Err": ""}


def md5sum(fname):
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


@view_config(route_name='docker_auth_plugin_activate', renderer='json')
def docker_auth_plugin_activate(request):
    logging.debug("Active plugin-godocker-auth plugin")
    return {"Implements": ["authz"]}


@view_config(route_name='docker_auth_plugin_AuthZReq', renderer='json')
def docker_auth_plugin_AuthZReq(request):
    logging.debug("Direct Active plugin-godocker-auth plugin")
    body = request.body
    if sys.version_info >= (3,):
        body = request.body.decode()

    data = json.loads(body, encoding=request.charset)
    if 'RequestBody' in data:
        decoded_data = base64.b64decode(data['RequestBody'])
        docker_data = json.loads(decoded_data)
        god_auth_token = None
        for docker_env in docker_data['Env']:
            if docker_env.startswith('GOD_AUTH_TOKEN='):
                god_auth_token = docker_env.replace('GOD_AUTH_TOKEN=', '')
                break
        if not god_auth_token:
            return {"Allow": False, "Msg": "No GOD_AUTH_TOKEN in env vars", "Err": "No GOD_AUTH_TOKEN in env vars"}
        decoded_msg = jwt.decode(god_auth_token, request.registry.god_config['secret_passphrase'], audience='urn:godocker/auth-api')
        logging.debug("GOD_AUTH_TOKEN: " + str(decoded_msg))
        decoded_msg = decoded_msg['docker']
        if decoded_msg['image'] != docker_data['Image']:
            return deny('Using different image')
        if 'network' in decoded_msg and decoded_msg['network'] and decoded_msg['network'] != docker_data['HostConfig']['NetworkMode']:
            return deny('Using a different network mode')
        if docker_data['HostConfig']['CapAdd']:
            return deny('Not allowed to extend capabilities')
        if docker_data['Entrypoint'][0] != decoded_msg['EntryPoint']:
            return deny('Not allowed to override Entrypoint')
        if docker_data['HostConfig']['Privileged']:
            return deny('Not allowed to execute in privileged mode')
        if docker_data['HostConfig']['Binds']:
            for bind in docker_data['HostConfig']['Binds']:
                if bind not in decoded_msg['volumes']:
                    return deny('Volume not allowed: ' + str(bind))
        if 'files' in decoded_msg:
            for md5_file in decoded_msg['files']:
                file_to_check = md5_file['file']
                file_to_check_md5 = md5sum(file_to_check)
                if file_to_check_md5 != md5_file['md5']:
                    return deny('Different files used')

    return {"Allow": True, "Msg": "test", "Err": ""}


@view_config(route_name='docker_auth_plugin_AuthZRes', renderer='json')
def docker_auth_plugin_AuthZRes(request):
    return {"Allow": True, "Msg": "test", "Err": ""}


@view_config(route_name='api_user_billing', renderer='json', request_method='GET')
def api_user_billing(request):
    '''
    .. http:get:: /api/1.0/(str:type)/(str:id)/billing/:year/:month

       Get user usage (self or admin) for month and year, per project, or a per project whatever the users if project belongs to user
       type=user/project
       Optional *force* query parameter to force a refresh and not using cache (force=0/1)

       :>json list: user usage { "_id" : { "user" : "XX", "project" : "default" }, "cpu" : 758, "ram" : 1031, 'gpus': 3, 'totaltime': 12434 }
       :statuscode 200: no error
    '''
    cfg = request.registry.god_config
    session_user = is_authenticated(request)
    if not session_user:
        return HTTPUnauthorized('Not authorized to access this resource')

    requested_user = request.matchdict['id']
    if not (_is_admin(request, session_user['id']) or session_user['id'] == requested_user):
        return HTTPUnauthorized('Not authorized to access this resource')

    dt = datetime.datetime.now()
    estimate = False
    if dt.month == int(request.matchdict['month']) and dt.year == int(request.matchdict['year']):
        estimate = True

    if request.matchdict['type'] == 'project':
        project_info = request.registry.db_mongo['projects'].find_one({'id': request.matchdict['id']})
        if project_info is None:
            return HTTPNotFound()
        if project_info['owner'] != session_user['id'] and not _is_admin(request, session_user['id']):
            return HTTPUnauthorized('Not authorized to access this resource')

    force = False
    if 'force' in request.params and request.params['force'] == '1':
        force = True

    begin_month = dt.replace(year=int(request.matchdict['year']), month=int(request.matchdict['month']), day=1, hour=0, minute=0, second=0)
    end_month = None
    if request.matchdict['month'] == '12':
        end_month = begin_month.replace(year=int(request.matchdict['year']) + 1, month=int(request.matchdict['month']), day=1, hour=0, minute=0, second=0)
    else:
        end_month = begin_month.replace(year=int(request.matchdict['year']), month=int(request.matchdict['month']) + 1, day=1, hour=0, minute=0, second=0)

    if not estimate:
        cache_redis = redis_call(request, request.registry.db_redis.get, cfg['redis_prefix'] + ':billing:' + request.matchdict['type'] + ':' + request.matchdict['id'] + ':' + request.matchdict['year'] + ':' + request.matchdict['month'])
        if cache_redis and not force:
            logging.error('use over cache')
            return json.loads(cache_redis)
        else:
            # User projects usage
            # usages = {'over': {}, 'running': {}}
            res = None
            if request.matchdict['type'] == 'user':
                res = request.registry.db_mongo['jobsover'].find({
                    'user.id': request.matchdict['id'],
                    '$and': [
                            {'status.date_over': {'$gt': time.mktime(begin_month.timetuple())}},
                            {'status.date_running': {'$lt': time.mktime(end_month.timetuple())}}
                    ]
                })
            else:
                res = request.registry.db_mongo['jobsover'].find({
                    'user.project': request.matchdict['id'],
                    '$and': [
                            {'status.date_over': {'$gt': time.mktime(begin_month.timetuple())}},
                            {'status.date_running': {'$lt': time.mktime(end_month.timetuple())}}
                    ]
                })

            projects = {}
            for match in res:
                # logging.error(str(match))
                if match['user']['project'] not in projects:
                    projects[match['user']['project']] = {'cpu': 0, 'gpus': 0, 'ram': 0, 'time': 0, 'count': 0}
                if match['status']['date_over'] > time.mktime(end_month.timetuple()):
                    match['status']['date_over'] = time.mktime(end_month.timetuple())
                if match['status']['date_running'] < time.mktime(begin_month.timetuple()):
                    match['status']['date_running'] = time.mktime(begin_month.timetuple())
                duration = match['status']['duration']
                if match['status']['duration'] > time.mktime(end_month.timetuple()) - time.mktime(begin_month.timetuple()):
                    duration = time.mktime(end_month.timetuple()) - time.mktime(begin_month.timetuple())
                projects[match['user']['project']]['cpu'] += match['requirements']['cpu']
                projects[match['user']['project']]['ram'] += match['requirements']['ram']
                projects[match['user']['project']]['gpus'] += match['requirements']['gpus']
                projects[match['user']['project']]['time'] += duration
                projects[match['user']['project']]['count'] += 1

            usage = []
            for project in projects:
                usage.append({'project': project, 'usage': projects[project]})

            redis_call(request, request.registry.db_redis.set, cfg['redis_prefix'] + ':billing:' + request.matchdict['type'] + ':' + request.matchdict['id'] + ':' + request.matchdict['year'] + ':' + request.matchdict['month'], json.dumps(usage))
            return usage
    else:
        cache_redis = redis_call(request, request.registry.db_redis.get, cfg['redis_prefix'] + ':billing:' + request.matchdict['type'] + ':' + request.matchdict['id'] + ':estimate')
        if cache_redis and not force:
            logging.error('use running cache')
            return json.loads(cache_redis)
        else:
            res = None
            if request.matchdict['type'] == 'user':
                res = request.registry.db_mongo['jobs'].find({
                    'user.id': request.matchdict['id'],
                    'status.primary': godutils.STATUS_RUNNING,
                    'status.date_running': {'$lt': time.mktime(end_month.timetuple())}
                })
            else:
                res = request.registry.db_mongo['jobsover'].find({
                    'user.project': request.matchdict['id'],
                    '$and': [
                            {'status.date_over': {'$gt': time.mktime(begin_month.timetuple())}},
                            {'status.date_running': {'$lt': time.mktime(end_month.timetuple())}}
                    ]
                })

            projects = {}
            for match in res:
                if match['user']['project'] not in projects:
                    projects[match['user']['project']] = {'cpu': 0, 'gpus': 0, 'ram': 0, 'time': 0, 'count': 0}
                if match['status']['date_running'] < time.mktime(begin_month.timetuple()):
                    match['status']['date_running'] = time.mktime(begin_month.timetuple())
                duration = time.mktime(dt.timetuple()) - match['status']['date_running']
                if duration > time.mktime(dt.timetuple()) - time.mktime(begin_month.timetuple()):
                    duration = time.mktime(dt.timetuple()) - time.mktime(begin_month.timetuple())
                projects[match['user']['project']]['cpu'] += match['requirements']['cpu']
                projects[match['user']['project']]['ram'] += match['requirements']['ram']
                projects[match['user']['project']]['gpus'] += match['requirements']['gpus']
                projects[match['user']['project']]['time'] += duration
                projects[match['user']['project']]['count'] += 1

            usage = []
            for project in projects:
                usage.append({'project': project, 'usage': projects[project]})

            redis_call(request, request.registry.db_redis.set, cfg['redis_prefix'] + ':billing:' + request.matchdict['type'] + ':' + request.matchdict['id'] + ':estimate', json.dumps(usage))
            redis_call(request, request.registry.db_redis.expire, cfg['redis_prefix'] + ':billing:' + request.matchdict['type'] + ':' + request.matchdict['id'] + ':estimate', 60 * 5)
            return usage
