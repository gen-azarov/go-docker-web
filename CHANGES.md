# 1.3.13

 * Add resource limitation per project
 * Optional *default* project removal (go-d.ini projects/is_default_allowed, defaults: true), if disabled task *must* select a project
 * Add /api/1.0/version to get modules version
 * In admin/dashboard menu, add link to each user page to allow admin to modify quota, prio, etc.
 * Fix live monitor (missing graph library)
 * Fix job replay when some ports requirements are set
 * Fix cookie secret
 * Fix display of projects quotas in user info page
 * Fix #39, Allow quota_time (user, project) to be set in seconds, minutes, hours, days (10s, 10m, 10h, 10d)
 * go-docker-live >= 1.3.4, display god.err in live logs along god.log

# 1.3.12

 * Fix mobile view for menu
 * Fix 33 add gpu quotas
 * Fix 34 checks quotas and prio are int , quotas are expressed in number per seconds
 * Fix directory navigation in files
 * Check primary status for new tasks
 * Add replay icon in running job tag
 * Allow project owner to manage project members and description
 * Allow project owner to mount any directory if *projects/owner_mount_any_dir* is set in config (new *projects* section in go-d.ini).
 * Fix osallou/go-docker #64, access job task files even if running
 * Fix case of wrong authorization header format
 * Fix query of user/project usage
 * Add optional *hooks* field to new tasks, hooks are called on job exit to *ok* or *ko* URLs with a POST request containing status information in body with JSON format
 * Fix gpus task requirements settings
 * Fix dashboard chart of ok/ko tasks
 * Feature 37, hide 'rejected by scheduler' from UI, message is misleading for user

*WARNING* quota values must be in seconds

# 1.3.11

 * Store dist directory in repo, closes #32

# 1.3.10

 * Fix angular-ui issue with "new job advance toggle"
 * fix missing jquery in Docker container

# 1.3.9

 * Remove dependency on angular-tour, not used
 * fix python3 decoding for live monitor, closes #58

# 1.3.8

 * On "View all users", show tasks of all users in Over tasks too
 * Add billing info API /api/1.0/user/(str:id)/billing/:year/:month
 * Update versions of bower components for manual installation to more recent versions
   * **WARNING**: for development, requires a *bower install* run.
 * Add failure history if present
 * Add bubble-chamber (https://github.com/osallou/sysdig-analyser) optional link (impacts production.ini bubble_chamber_url_link, see production.ini.example)
 * Fix label color on failed jobs
 * Switch executor related tags to meta/executor task fields
 * Add /archive/{days} API and web button for user to archive all jobs older than X days
 * Add job archive deletion API (delete files but also task from database)

# 1.3.7

 * Check requirements.executor failure_policyd on new task submission for backward compatibility
 * update swagger definition for API (change some tags, add missing pagination fields in list queries)
 * wrap text for failure raison in UI
 * Fix pagination when requesting to jobs of all users

# 1.3.6

* Fix production.ini.example for redirection when using https to take into account X-Forwarded-Proto header
* Fix gitignore , socket.io was not included lead to *live* failure in containers or prod environements.
* Fix missing swagger api in prod environment if not build manually with grunt
* GODWEB-5 Allow sharing access to a task info and task files via a public URL
* Use latest swagger 2 version to fix api loading
* set default cpu/ram/gpus values if not set in task requirements at task creation using config defaults
* Fix time display to match 2 digits

# 1.3.5

* Fix job arrays
* Tables layout updates

# 1.3.4

* Fix pagination when searching in finished jobs
* Manage pagination in active jobs ourselves to speedup loading and large list management
* Add GPU management with Mesos (needs go-docker >= 1.3.2)
* Layout/display updates
* Add swagger API with access from menu in resources
* Fix image selection
* Fix jobs page reload issues
* Add redis authentication with optional param redis_password in go-d.ini

# 1.3.3

* Fix case with CLI when executor not specified
* Fix HTML layout

# 1.3.2

* Add optional session_timeout in go-d.ini to specify duration of web sessions (in seconds)
* Do not add executor tags if already present
* Fix user info reload at login
* Set container.id in task to null when creating tasks (issue with CLI)

# 1.3.1

* Fix interactive jobs. An error on web interface prevents interactive jobs

# 1.3

* Add multi executor support, needs go-docker >= 1.3
* Add optional hostname to prometheus stats
* Fix prometheus + gunicorn multiprocess metrics
  * Modifies startup command and needs a temp directory

#1.2

* GODWEB-4 optionally view tasks of users in same project
  * New go-d.ini parameter "share_project_tasks", need to be True to view other project member tasks
* Allow proxy authentication using REMOTE_USER header
  * New go-d.ini parameter allow_proxy_remote_user to allow this kind of authentication
* Remove image usage increment from web and switch it to godocker watchers, to add image only when job could be started
* Fix #20 add different label color if system error is detected (status.reason set and exitcode > 0 && != 137)
* Add link to files in list of jobs over
* GOD-51 Support CNI networks
* GOD-35 Support Mesos unified containerizer, needs go-docker >= 1.2   
* Fix #17 Script content in task info doesn't appear
* Fix #24 Resources button go to a blank page
* Fix #22 Create a "New Job" button in menu
* Fix #23 Create an advanced section in *new job* form
* Fix browse of job files
* Add recipe removal
* Add a link to images to their web page with new go-d.ini param link_image_to_registry
* Fix admin access to recipes
* Fix #21 UI improvements
* Fix refresh of live logs
* Refactor display of jobs over with remote pagination to be able to look at all previous jobs
* Fix #25 Add route prefix option with prefix parameter in development/production.ini file or env variable GODOCKER_WEB_PREFIX

# 1.1.2

* GOD-45 Support linux group management in projects
* Add resource page to see used resources on cluster
* Get user quota from auth plugins
* Add possibility to open additional ports
* GODWEB-3 add marketplace recipes
* fix logout issue
* Add dynamic reload of config via go-d-scheduler config-reload command
* Display/browse user personal storage in user preferences
* fix pending jobs count to avoid some race conditions
* GOD-38 Add remote applications token management
* GOD-37 Support data requirements in tasks
* Add new HTTP authorization mode to manages tasks by a remote application:
  * Authorization: token XXX, with XXX a remote application token
* Add *tag* query parameter to "/api/1.0/user/{id}/task" to kill all user tasks matching tag.
* GOD-44 add restart strategy in case of node failure, new task requirement field (optional) failure_policy=N
* GOD-46 let user archive a job over

# 1.1.1

* Fix tmpstorage tests
* Manage "enter" keypress on login page

# 1.1

* GOD-10 allow use a script with an interactive session, script executed before ssh server
* GODWEB-1 manage multiple instances for constraints

# 1.0.1

* add swagger API doc/godocker.json
* use iStatus monitoring in admin interface (etcd, etc.)
* add apikey renew in API
* add Temporary local storage (see go-docker)
* optional *guest* support, i.e. accepting users connecting with Google, GitHub, ... and not in system. Guest will map to a system user for execution.

# 1.0

* first release
